
package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.commons.spi.ProcessorInfo;
import cz.utb.fai.apac.commons.util.ScoreUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

/**
 * @author frantisek
 */
@ProcessorInfo(language = Language.PYTHON, version = 1.0)
public class PythonProcessor implements Processor {

  private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class);
  private static final String EXECUTE_CMD = "python %s";

  @Override
  public boolean isCompilationRequired() {
    return false;
  }

  @Override
  public String compileCommand(File submissionWorkspace) {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public boolean compilationSuccess(CompileResult compileResult) {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public String executionCommand(File submissionWorkspace) {
    Collection<File> allFiles = FileUtils.listFiles(submissionWorkspace, new String[]{"py"}, true);

    String exeFile = "";

    if (allFiles.size() > 1) {
      try {
        File mainFile = null;
        for (File f : allFiles) {
          String fileContent = FileUtils.readFileToString(f);

          if (fileContent.contains("__main__")) {
            mainFile = f;
            break;
          }
        }

        if (mainFile != null) {
          exeFile = mainFile.getName();
        }
      } catch (IOException ex) {
        LOGGER.error("Exception during preparing execution", ex);
      }
    } else if (!allFiles.isEmpty()) {
      exeFile = allFiles.iterator().next().getName();
    }

    return exeFile.isEmpty()
        ? ""
        : String.format(EXECUTE_CMD, exeFile);
  }

  @Override
  public boolean executionSuccess(ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Execution result cannot be null");
    boolean success = true;

    if (StringUtils.isNotBlank(executionResult.getOutput())
        && (executionResult.getOutput().contains("killed") 
        || executionResult.getOutput().contains(">>>"))) {
      success &= false;
    }
    return success;
  }

  @Override
  public int calcCompilationScore(CompileResult compileResult) {
    return 0;
  }

  @Override
  public double calcExecutionScore(ExecutionResult executionResult) {

    String referenceOutput = executionResult.getTestVector().getReferenceOutput();
    double points = executionResult.getTestVector().getMaxPoints();

    if (referenceOutput != null && !referenceOutput.isEmpty()) {
      double similarity = ScoreUtil.calcOutputSimilarity(executionResult.getOutput(), 
          referenceOutput);
      LOGGER.info(String.format("Similarity is %s", similarity));

      if (similarity < (executionResult.getOutputThreshold() / 100.0)) {
        similarity = 0;

      }
      points *= similarity;
    } else {
      points = 0;
    }
    return points;
  }
}
