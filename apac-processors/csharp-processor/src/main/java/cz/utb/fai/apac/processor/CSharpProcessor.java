
package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.enums.Extension;
import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.commons.spi.ProcessorInfo;
import cz.utb.fai.apac.commons.util.DirFilter;
import cz.utb.fai.apac.commons.util.FileUtil;
import cz.utb.fai.apac.commons.util.GenericExtFilter;
import cz.utb.fai.apac.commons.util.ScoreUtil;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of {@link Processor} for C# programming language. Implementation is based on Mono
 * project.
 *
 * @author frantisek
 */
@ProcessorInfo(language = Language.CSHARP, version = 1.0)
public class CSharpProcessor implements Processor {

  private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class);
  private static final String COMPILER_CSHARP_COMMAND = "gmcs %s -out:%s";
  private static final String EXECUTE_CMD = "mono %s.%s";
  private static final GenericExtFilter SOURCE_FILES_FILTER = new GenericExtFilter(
      new String[]{"cs"});

  @Override
  public boolean isCompilationRequired() {
    return true;
  }

  @Override
  public String compileCommand(File submissionWorkspace) {
    File[] sourcesDirs = submissionWorkspace.listFiles(new DirFilter());

    if (sourcesDirs.length > 0) {
      try {
        FileUtil.getFilesFromDirs(submissionWorkspace, sourcesDirs, SOURCE_FILES_FILTER);
      } catch (IOException ex) {
        LOGGER.error("Copy files from dir exception", ex);
      }
    }
    String outputFile = String.format("%s.%s", submissionWorkspace.getName(),
        Extension.BUILD.toString());

    String fileList = getFileList(submissionWorkspace);
    return fileList.isEmpty()
        ? "" : String.format(COMPILER_CSHARP_COMMAND, fileList, outputFile);
  }

  @Override
  public boolean compilationSuccess(CompileResult compileResult) {
    return !compileResult.getOutput().contains("error");
  }

  @Override
  public String executionCommand(File submissionWorkspace) {
    return String.format(EXECUTE_CMD, submissionWorkspace.getName(), Extension.BUILD.toString());
  }

  @Override
  public boolean executionSuccess(ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Execution result cannot be null");

    return !(StringUtils.isNotBlank(executionResult.getOutput())
        && (executionResult.getOutput().contains("killed")));
  }

  @Override
  public int calcCompilationScore(CompileResult compileResult) {
    return 0;
  }

  @Override
  public double calcExecutionScore(ExecutionResult executionResult) {

    String referenceOutput = executionResult.getTestVector().getReferenceOutput();
    double points = executionResult.getTestVector().getMaxPoints();

    if (referenceOutput != null && !referenceOutput.isEmpty()) {
      double similarity = ScoreUtil.calcOutputSimilarity(executionResult.getOutput(),
          referenceOutput);
      LOGGER.info(String.format("Similarity is %s", similarity));

      if (similarity < (executionResult.getOutputThreshold() / 100.0)) {
        similarity = 0;

      }
      points *= similarity;
    } else {
      points = 0;
    }
    return points;
  }

  private String getFileList(File workspace) {
    Set<String> sourceFiles = new HashSet<>(Arrays.asList(workspace.list(SOURCE_FILES_FILTER)));

    StringBuilder sb = new StringBuilder();
    sourceFiles.stream().map((s) -> {
      sb.append(s);
      return s;
    }).forEach((_item) -> {
      sb.append("");
    });

    return sb.toString();
  }
}
