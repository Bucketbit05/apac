
package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.commons.spi.ProcessorInfo;
import cz.utb.fai.apac.commons.util.ScoreUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Implementation of {@link Processor} for Java programming language. Implementation only supports
 * Ant projects.
 *
 * @author frantisek
 */
@ProcessorInfo(language = Language.JAVA, version = 1.0)
public class JavaProcessor implements Processor {

  private static final Logger LOGGER = LoggerFactory.getLogger(Processor.class);
  private static final String ANT_CMD = "ant -q -buildfile %s";
  private static final String BUILD_XML_NAME = "build.xml";
  private static final String JAVA_EXEC_CMD = "java -jar %s";

  @Override
  public boolean isCompilationRequired() {
    return true;
  }

  @Override
  public String compileCommand(File submissionWorkspace) {
    List<File> allFiles = new ArrayList<>(FileUtils.listFiles(submissionWorkspace,
        new RegexFileFilter("^(.*?)"), DirectoryFileFilter.DIRECTORY));

    Optional<File> file = allFiles.stream()
        .filter(f -> f.getName().equals(BUILD_XML_NAME)).findFirst();

    String buildXMLPath = file.isPresent() ? file.get().getAbsolutePath() : "";
    return buildXMLPath.isEmpty()
        ? ""
        : String.format(ANT_CMD, buildXMLPath);
  }

  @Override
  public boolean compilationSuccess(CompileResult compileResult) {
    Objects.requireNonNull(compileResult, "Compile result cannot be null");

    boolean success = true;

    if (StringUtils.isNotBlank(compileResult.getOutput())
        && compileResult.getOutput().contains("error")) {
      success &= false;
    }
    return success;
  }

  @Override
  public String executionCommand(File submissionWorkspace) {
    List<File> allFiles = new ArrayList<>(FileUtils
        .listFiles(submissionWorkspace, new RegexFileFilter("^(.*?)"),
            DirectoryFileFilter.DIRECTORY));
    Optional<File> file = allFiles.stream().filter(f -> f.getName().equals("dist")
        && f.isDirectory()).findFirst();

    File distFolder = file.isPresent() ? file.get() : submissionWorkspace;

    File[] jarFiles = distFolder.listFiles((File dir, String name) -> name.endsWith("jar"));

    String command = "";
    if (jarFiles.length > 0) {
      command = String.format(JAVA_EXEC_CMD, jarFiles[0]);
    }

    return command;
  }

  @Override
  public boolean executionSuccess(ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Execution result cannot be null");
    boolean success = true;

    if (StringUtils.isNotBlank(executionResult.getOutput())
        && (executionResult.getOutput().contains("killed")
        || executionResult.getOutput().contains("error"))) {
      success &= false;
    }
    return success;
  }

  @Override
  public int calcCompilationScore(CompileResult compileResult) {
    Objects.requireNonNull(compileResult, "Compile result cannot be null");
    return StringUtils.isNotBlank(compileResult.getOutput())
        ? calcWarnings(compileResult.getOutput()) : 0;
  }

  @Override
  public double calcExecutionScore(ExecutionResult executionResult) {

    String referenceOutput = executionResult.getTestVector().getReferenceOutput();
    double points = executionResult.getTestVector().getMaxPoints();

    if (referenceOutput != null && !referenceOutput.isEmpty()) {
      double similarity = ScoreUtil.calcOutputSimilarity(executionResult.getOutput(),
          referenceOutput);
      LOGGER.info(String.format("Similarity is %s", similarity));

      if (similarity < (executionResult.getOutputThreshold() / 100.0)) {
        similarity = 0;

      }
      points *= similarity;
    } else {
      points = 0;
    }
    return points;
  }

  private int calcWarnings(String compilerResult) {
    int warnings = 0;
    Scanner scanner = new Scanner(compilerResult);
    while (scanner.hasNextLine()) {
      if (scanner.nextLine().contains("warning")) {
        warnings++;
      }
    }
    return warnings;
  }

}
