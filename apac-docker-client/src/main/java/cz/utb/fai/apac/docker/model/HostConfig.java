/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Frantisek Spacek
 */
public class HostConfig {

  @SerializedName("Binds")
  private String[] binds;

  @SerializedName("Links")
  private String[] links;

  @SerializedName("LxcConf")
  private Map<String, String> LXCConf;

  @SerializedName("Memory")
  private long memoryLimit = 0;

  @SerializedName("MemorySwap")
  private long memorySwap = 0;

  @SerializedName("CpuShares")
  private int cpuShares = 0;

  @SerializedName("CpuPeriod")
  private int cpuPeriod = 0;

  @SerializedName("CpusetCpus")
  private String cpusetCpus;

  @SerializedName("CpusetMems")
  private String cpusetMems;

  @SerializedName("BlkioWeight")
  private int blkioWeight;

  @SerializedName("OomKillDisable")
  private boolean oomKillDisable = false;

  @SerializedName("PortBindings")
  private Map<String, NetworkPort[]> portBindings;

  @SerializedName("PublishAllPorts")
  private boolean publishAllPorts = false;

  @SerializedName("Privileged")
  private boolean privileged = false;

  @SerializedName("ReadonlyRootfs")
  private boolean readOnlyRootFS = false;

  @SerializedName("Dns")
  private String[] dns;

  @SerializedName("DnsSearch")
  private String[] dnsSearch;

  @SerializedName("ExtraHosts")
  private String[] extraHosts;

  @SerializedName("VolumesFrom")
  private String[] volumesFrom;

  @SerializedName("CapAdd")
  private String[] capAdd;

  @SerializedName("CapDrop")
  private String[] capDrop;

  @SerializedName("RestartPolicy")
  private Map<String, ?> restartPolicy;

  @SerializedName("NetworkMode")
  private String networkMode;

  @SerializedName("Devices")
  private Map<String, ?> devices;

  @SerializedName("Ulimits")
  private List<Ulimit> ulimits = new ArrayList<>();

  @SerializedName("SecurityOpt")
  private String[] securityOpt;

  public HostConfig(String[] binds, String[] links,
      Map<String, String> LXCConf, String cpusetCpus, String cpusetMems,
      int blkioWeight, Map<String, NetworkPort[]> portBindings,
      String[] dns, String[] dnsSearch, String[] extraHosts,
      String[] volumesFrom, String[] capAdd, String[] capDrop,
      Map<String, ?> restartPolicy, String networkMode,
      Map<String, ?> devices, String[] securityOpt) {
    this.binds = binds;
    this.links = links;
    this.LXCConf = LXCConf;
    this.cpusetCpus = cpusetCpus;
    this.cpusetMems = cpusetMems;
    this.blkioWeight = blkioWeight;
    this.portBindings = portBindings;
    this.dns = dns;
    this.dnsSearch = dnsSearch;
    this.extraHosts = extraHosts;
    this.volumesFrom = volumesFrom;
    this.capAdd = capAdd;
    this.capDrop = capDrop;
    this.restartPolicy = restartPolicy;
    this.networkMode = networkMode;
    this.devices = devices;
    this.securityOpt = securityOpt;
  }

  public String[] getBinds() {
    return binds;
  }

  public void setBinds(String[] binds) {
    this.binds = binds;
  }

  public String[] getLinks() {
    return links;
  }

  public void setLinks(String[] links) {
    this.links = links;
  }

  public Map<String, String> getLXCConf() {
    return LXCConf;
  }

  public void setLXCConf(Map<String, String> LXCConf) {
    this.LXCConf = LXCConf;
  }

  public boolean isPrivileged() {
    return privileged;
  }

  public void setPrivileged(boolean privileged) {
    this.privileged = privileged;
  }

  public boolean isPublishAllPorts() {
    return publishAllPorts;
  }

  public void setPublishAllPorts(boolean publishAllPorts) {
    this.publishAllPorts = publishAllPorts;
  }

  public Map<String, NetworkPort[]> getPortBindings() {
    return portBindings;
  }

  public void setPortBindings(Map<String, NetworkPort[]> portBindings) {
    this.portBindings = portBindings;
  }

  public String[] getDns() {
    return dns;
  }

  public void setDns(String[] dns) {
    this.dns = dns;
  }

  public String[] getDnsSearch() {
    return dnsSearch;
  }

  public void setDnsSearch(String[] dnsSearch) {
    this.dnsSearch = dnsSearch;
  }

  public String[] getVolumesFrom() {
    return volumesFrom;
  }

  public void setVolumesFrom(String[] volumesFrom) {
    this.volumesFrom = volumesFrom;
  }

  public String[] getCapAdd() {
    return capAdd;
  }

  public void setCapAdd(String[] capAdd) {
    this.capAdd = capAdd;
  }

  public String[] getCapDrop() {
    return capDrop;
  }

  public void setCapDrop(String[] capDrop) {
    this.capDrop = capDrop;
  }

  public Map<String, ?> getRestartPolicy() {
    return restartPolicy;
  }

  public void setRestartPolicy(Map<String, ?> restartPolicy) {
    this.restartPolicy = restartPolicy;
  }

  public String getNetworkMode() {
    return networkMode;
  }

  public void setNetworkMode(String networkMode) {
    this.networkMode = networkMode;
  }

  public Map<String, ?> getDevices() {
    return devices;
  }

  public void setDevices(Map<String, ?> devices) {
    this.devices = devices;
  }

  public long getMemoryLimit() {
    return memoryLimit;
  }

  public void setMemoryLimit(long memoryLimit) {
    this.memoryLimit = memoryLimit;
  }

  public long getMemorySwap() {
    return memorySwap;
  }

  public void setMemorySwap(long memorySwap) {
    this.memorySwap = memorySwap;
  }

  public int getCpuShares() {
    return cpuShares;
  }

  public void setCpuShares(int cpuShares) {
    this.cpuShares = cpuShares;
  }

  public int getCpuPeriod() {
    return cpuPeriod;
  }

  public void setCpuPeriod(int cpuPeriod) {
    this.cpuPeriod = cpuPeriod;
  }

  public String getCpusetCpus() {
    return cpusetCpus;
  }

  public void setCpusetCpus(String cpusetCpus) {
    this.cpusetCpus = cpusetCpus;
  }

  public String getCpusetMems() {
    return cpusetMems;
  }

  public void setCpusetMems(String cpusetMems) {
    this.cpusetMems = cpusetMems;
  }

  public int getBlkioWeight() {
    return blkioWeight;
  }

  public void setBlkioWeight(int blkioWeight) {
    this.blkioWeight = blkioWeight;
  }

  public boolean isOomKillDisable() {
    return oomKillDisable;
  }

  public void setOomKillDisable(boolean oomKillDisable) {
    this.oomKillDisable = oomKillDisable;
  }

  public boolean isReadOnlyRootFS() {
    return readOnlyRootFS;
  }

  public void setReadOnlyRootFS(boolean readOnlyRootFS) {
    this.readOnlyRootFS = readOnlyRootFS;
  }

  public String[] getExtraHosts() {
    return extraHosts;
  }

  public void setExtraHosts(String[] extraHosts) {
    this.extraHosts = extraHosts;
  }

  public List<Ulimit> getUlimits() {
    return ulimits;
  }

  public void setUlimits(List<Ulimit> ulimits) {
    this.ulimits = ulimits;
  }

  public String[] getSecurityOpt() {
    return securityOpt;
  }

  public void setSecurityOpt(String[] securityOpt) {
    this.securityOpt = securityOpt;
  }

  @Override
  public String toString() {
    return "HostConfig{" + "binds=" + Arrays.toString(binds)
        + ", links=" + Arrays.toString(links)
        + ", LXCConf=" + LXCConf + ", privileged="
        + privileged + ", publishAllPorts=" + publishAllPorts
        + ", portBindings=" + portBindings
        + ", dns=" + Arrays.toString(dns)
        + ", dnsSearch=" + Arrays.toString(dnsSearch)
        + ", volumesFrom=" + Arrays.toString(volumesFrom)
        + ", capAdd=" + Arrays.toString(capAdd)
        + ", capDrop=" + Arrays.toString(capDrop)
        + ", restartPolicy=" + restartPolicy + ", networkMode="
        + networkMode + '}';
  }

  public static class Builder {

    private String[] binds = null;
    private String[] links = null;
    private Map<String, String> LXCConf = null;
    private String cpusetCpus = null;
    private String cpusetMems = null;
    private int blkioWeight = 0;
    private Map<String, NetworkPort[]> portBindings = null;
    private String[] dns = null;
    private String[] dnsSearch = null;
    private String[] extraHosts = null;
    private String[] volumesFrom = null;
    private String[] capAdd = null;
    private String[] capDrop = null;
    private Map<String, ?> restartPolicy = null;
    private String networkMode = null;
    private Map<String, ?> devices = null;
    private String[] securityOpt = null;
    private int cpuShares;
    private int memoryLimit;
    private int memorySwap;

    public Builder() {
    }

    public Builder setBinds(String[] binds) {
      this.binds = binds;
      return this;
    }

    public Builder setLinks(String[] links) {
      this.links = links;
      return this;
    }

    public Builder setLXCConf(Map<String, String> LXCConf) {
      this.LXCConf = LXCConf;
      return this;
    }

    public Builder setCpusetCpus(String cpusetCpus) {
      this.cpusetCpus = cpusetCpus;
      return this;
    }

    public Builder setCpusetMems(String cpusetMems) {
      this.cpusetMems = cpusetMems;
      return this;
    }

    public Builder setBlkioWeight(int blkioWeight) {
      this.blkioWeight = blkioWeight;
      return this;
    }

    public Builder setPortBindings(Map<String, NetworkPort[]> portBindings) {
      this.portBindings = portBindings;
      return this;
    }

    public Builder setDns(String[] dns) {
      this.dns = dns;
      return this;
    }

    public Builder setDnsSearch(String[] dnsSearch) {
      this.dnsSearch = dnsSearch;
      return this;
    }

    public Builder setExtraHosts(String[] extraHosts) {
      this.extraHosts = extraHosts;
      return this;
    }

    public Builder setVolumesFrom(String[] volumesFrom) {
      this.volumesFrom = volumesFrom;
      return this;
    }

    public Builder setCapAdd(String[] capAdd) {
      this.capAdd = capAdd;
      return this;
    }

    public Builder setCapDrop(String[] capDrop) {
      this.capDrop = capDrop;
      return this;
    }

    public Builder setRestartPolicy(Map<String, ?> restartPolicy) {
      this.restartPolicy = restartPolicy;
      return this;
    }

    public Builder setNetworkMode(String networkMode) {
      this.networkMode = networkMode;
      return this;
    }

    public Builder setDevices(Map<String, ?> devices) {
      this.devices = devices;
      return this;
    }

    public Builder setSecurityOpt(String[] securityOpt) {
      this.securityOpt = securityOpt;
      return this;
    }

    public Builder setCpuShares(int cpuShare) {
      this.cpuShares = cpuShare;
      return this;
    }

    public Builder setMemoryLimit(int memoryLimit) {
      this.memoryLimit = memoryLimit;
      return this;
    }

    public Builder setMemorySwap(int memoryWithWap) {
      this.memorySwap = memoryWithWap;
      return this;
    }

    public HostConfig build() {
      HostConfig config = new HostConfig(binds, links, LXCConf, cpusetCpus,
          cpusetMems, blkioWeight, portBindings, dns, dnsSearch,
          extraHosts, volumesFrom, capAdd, capDrop, restartPolicy,
          networkMode, devices, securityOpt);
      config.setMemoryLimit(memoryLimit);
      config.setCpuShares(cpuShares);
      config.setMemorySwap(memorySwap);

      return config;
    }

  }

}
