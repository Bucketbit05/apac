/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.client;

import cz.utb.fai.apac.docker.exception.DockerErrorException;
import cz.utb.fai.apac.docker.exception.DockerNotFoundException;
import cz.utb.fai.apac.docker.exception.InvalidRequestException;
import cz.utb.fai.apac.docker.filter.ListFilter;
import cz.utb.fai.apac.docker.filter.LogsFilter;
import cz.utb.fai.apac.docker.model.Container;
import cz.utb.fai.apac.docker.model.ContainerConfig;
import cz.utb.fai.apac.docker.model.ContainerDetail;
import cz.utb.fai.apac.docker.model.ContainerInfo;
import cz.utb.fai.apac.docker.model.DockerInfo;
import cz.utb.fai.apac.docker.model.DockerVersion;
import cz.utb.fai.apac.docker.model.StartConfig;

import java.io.InputStream;
import java.util.List;

/**
 * Docker engine REST client.
 *
 * @author František Špaček
 */
public interface DockerClient {

  /**
   * Ping docker engine, to check if engine is running.
   *
   * @throws DockerErrorException if docker error occurs
   */
  void ping() throws
      DockerErrorException;

  /**
   * Gets info about docker engine.
   *
   * @return
   */
  DockerInfo info() throws DockerErrorException;

  /**
   * Gets version info about docker engine.
   *
   * @return
   */
  DockerVersion version() throws DockerErrorException;

  /**
   * Gets list of containers based on provided filter.
   *
   * @param filter
   * @return
   * @throws InvalidRequestException
   * @throws DockerErrorException
   */
  List<Container> get(ListFilter filter) throws InvalidRequestException, DockerErrorException;

  /**
   *
   * @param id
   * @return
   * @throws DockerNotFoundException
   * @throws DockerErrorException
   */
  ContainerDetail inspect(String id) throws DockerNotFoundException, DockerErrorException;

  ContainerInfo create(ContainerConfig config) throws InvalidRequestException, DockerErrorException;

  /**
   *
   * @param id
   * @param config
   * @throws DockerNotFoundException
   * @throws InvalidRequestException
   * @throws DockerErrorException
   */
  void start(String id, StartConfig config) throws DockerNotFoundException,
      InvalidRequestException,
      DockerErrorException;

  /**
   *
   * @param id
   * @throws DockerNotFoundException
   * @throws DockerErrorException
   */
  void kill(String id) throws DockerNotFoundException, DockerErrorException;

  /**
   *
   * @param id
   * @throws DockerNotFoundException
   * @throws DockerErrorException
   */
  void stop(String id) throws DockerNotFoundException, DockerErrorException;

  /**
   *
   * @param id
   * @throws DockerNotFoundException
   * @throws DockerErrorException
   */
  void delete(String id) throws DockerNotFoundException, DockerErrorException;

  /**
   *
   * @param id
   * @param filter
   * @return
   * @throws DockerNotFoundException
   * @throws InvalidRequestException
   * @throws DockerErrorException
   */
  InputStream getLogs(String id, LogsFilter filter)
      throws DockerNotFoundException, InvalidRequestException,
      DockerErrorException;

  void pull(String image) throws DockerNotFoundException, DockerErrorException;
}
