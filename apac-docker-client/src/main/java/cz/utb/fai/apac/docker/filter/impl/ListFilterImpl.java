/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.filter.impl;

import cz.utb.fai.apac.docker.filter.ListFilter;

/**
 *
 * @author František Špaček
 */
public class ListFilterImpl implements ListFilter {

  private final boolean all;
  private final Integer limit;
  private final String sinceId;
  private final String beforeId;
  private final boolean showSize;
  private final String status;
  private final Integer exitCode;

  public ListFilterImpl(boolean all, Integer limit, String sinceId,
      String beforeId, boolean showSize, String status,
      Integer exitCode) {
    this.all = all;
    this.limit = limit;
    this.sinceId = sinceId;
    this.beforeId = beforeId;
    this.showSize = showSize;
    this.status = status;
    this.exitCode = exitCode;
  }

  @Override
  public boolean getAll() {
    return all;
  }

  @Override
  public Integer getLimit() {
    return limit;
  }

  @Override
  public String getSinceId() {
    return sinceId;
  }

  @Override
  public String getBeforeId() {
    return beforeId;
  }

  @Override
  public boolean showSize() {
    return showSize;
  }

  @Override
  public String getStatus() {
    return status;
  }

  @Override
  public Integer getExitedCode() {
    return exitCode;
  }

}
