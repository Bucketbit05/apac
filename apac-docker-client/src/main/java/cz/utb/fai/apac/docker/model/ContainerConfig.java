/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;
import java.util.Map;

/**
 * @author Frantisek Spacek
 * @todo upgrade to new api version
 */
public class ContainerConfig {

  @SerializedName("Hostname")
  private String hostName;

  @SerializedName("Domainname")
  private String domainName;

  @SerializedName("User")
  private String user;

  @SerializedName("Tty")
  private boolean tty = false;

  @SerializedName("OpenStdin")
  private boolean stdinOpen;

  @SerializedName("StdinOnce")
  private boolean stdInOnce;

  @SerializedName("AttachStdin")
  private boolean attachStdin = false;

  @SerializedName("AttachStdout")
  private boolean attachStdout = false;

  @SerializedName("AttachStderr")
  private boolean attachStderr = false;

  @SerializedName("Env")
  private String[] env;

  @SerializedName("Cmd")
  private String[] cmd;

  @SerializedName("Image")
  private String image;

  @SerializedName("Volumes")
  private Map<String, ?> volumes;

  @SerializedName("Entrypoint")
  private String[] entrypoint = new String[]{};

  @SerializedName("NetworkDisabled")
  private boolean networkDisabled = false;

  @SerializedName("Privileged")
  private boolean privileged = false;

  @SerializedName("WorkingDir")
  private String workingDir = "";

  @SerializedName("MacAddress")
  private String macAddress;

  @SerializedName("ExposedPorts")
  private Map<String, ?> exposedPorts;

  @SerializedName("HostConfig")
  private HostConfig hostConfig;

  public ContainerConfig() {
  }

  public ContainerConfig(String hostName, String domainName,
      String user, boolean tty, boolean stdinOpen, boolean stdInOnce,
      boolean attachStdin, boolean attachStdout, boolean attachStderr,
      String[] env, String[] cmd, String image, Map<String, ?> volumes,
      String[] entrypoint, boolean networkDisabled, boolean privileged,
      Map<String, ?> exposedPorts,
      HostConfig hostConfig) {
    this.hostName = hostName;
    this.domainName = domainName;
    this.user = user;
    this.tty = tty;
    this.stdinOpen = stdinOpen;
    this.stdInOnce = stdInOnce;
    this.attachStdin = attachStdin;
    this.attachStdout = attachStdout;
    this.attachStderr = attachStderr;
    this.env = env;
    this.cmd = cmd;
    this.image = image;
    this.volumes = volumes;
    this.entrypoint = entrypoint;
    this.networkDisabled = networkDisabled;
    this.privileged = privileged;
    this.exposedPorts = exposedPorts;
    this.hostConfig = hostConfig;
  }

  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public boolean isTty() {
    return tty;
  }

  public void setTty(boolean tty) {
    this.tty = tty;
  }

  public boolean isStdinOpen() {
    return stdinOpen;
  }

  public void setStdinOpen(boolean stdinOpen) {
    this.stdinOpen = stdinOpen;
  }

  public boolean isStdInOnce() {
    return stdInOnce;
  }

  public void setStdInOnce(boolean stdInOnce) {
    this.stdInOnce = stdInOnce;
  }

  public boolean isAttachStdin() {
    return attachStdin;
  }

  public void setAttachStdin(boolean attachStdin) {
    this.attachStdin = attachStdin;
  }

  public boolean isAttachStdout() {
    return attachStdout;
  }

  public void setAttachStdout(boolean attachStdout) {
    this.attachStdout = attachStdout;
  }

  public boolean isAttachStderr() {
    return attachStderr;
  }

  public void setAttachStderr(boolean attachStderr) {
    this.attachStderr = attachStderr;
  }

  public String[] getEnv() {
    return env;
  }

  public void setEnv(String[] env) {
    this.env = env;
  }

  public String[] getCmd() {
    return cmd;
  }

  public void setCmd(String[] cmd) {
    this.cmd = cmd;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Map<String, ?> getVolumes() {
    return volumes;
  }

  public void setVolumes(Map<String, ?> volumes) {
    this.volumes = volumes;
  }

  public String[] getEntrypoint() {
    return entrypoint;
  }

  public void setEntrypoint(String[] entrypoint) {
    this.entrypoint = entrypoint;
  }

  public boolean isNetworkDisabled() {
    return networkDisabled;
  }

  public void setNetworkDisabled(boolean networkDisabled) {
    this.networkDisabled = networkDisabled;
  }

  public boolean isPrivileged() {
    return privileged;
  }

  public void setPrivileged(boolean privileged) {
    this.privileged = privileged;
  }

  public String getWorkingDir() {
    return workingDir;
  }

  public void setWorkingDir(String workingDir) {
    this.workingDir = workingDir;
  }

  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public Object getExposedPorts() {
    return exposedPorts;
  }

  public void setExposedPorts(Map<String, ?> exposedPorts) {
    this.exposedPorts = exposedPorts;
  }

  public HostConfig getHostConfig() {
    return hostConfig;
  }

  public String getMacAddress() {
    return macAddress;
  }

  public void setMacAddress(String macAddress) {
    this.macAddress = macAddress;
  }

  public void setHostConfig(HostConfig hostConfig) {
    this.hostConfig = hostConfig;
  }

  public static class Builder {

    private String hostName = null;
    private String workingDir = null;
    private String domainName = null;
    private String user = null;
    private boolean tty = false;
    private boolean stdinOpen = false;
    private boolean stdInOnce = false;
    private boolean attachStdin = false;
    private boolean attachStdout = false;
    private boolean attachStderr = false;
    private String[] env = null;
    private String[] cmd = null;
    private String[] dns = null;
    private String image;
    private Map<String, ?> volumes = null;
    private String[] entrypoint = new String[]{};
    private boolean networkDisabled = false;
    private boolean privileged = false;
    private Map<String, ?> exposedPorts = null;
    private HostConfig hostConfig = null;

    public Builder() {
    }

    public Builder setHostName(String hostName) {
      this.hostName = hostName;
      return this;
    }

    public Builder setDomainName(String domainName) {
      this.domainName = domainName;
      return this;
    }

    public Builder setUser(String user) {
      this.user = user;
      return this;
    }

    public Builder setTty(boolean tty) {
      this.tty = tty;
      return this;
    }

    public Builder setStdinOpen(boolean stdinOpen) {
      this.stdinOpen = stdinOpen;
      return this;
    }

    public Builder setStdInOnce(boolean stdInOnce) {
      this.stdInOnce = stdInOnce;
      return this;
    }

    public Builder setAttachStdin(boolean attachStdin) {
      this.attachStdin = attachStdin;
      return this;
    }

    public Builder setAttachStdout(boolean attachStdout) {
      this.attachStdout = attachStdout;
      return this;
    }

    public Builder setAttachStderr(boolean attachStderr) {
      this.attachStderr = attachStderr;
      return this;
    }

    public Builder setEnv(String[] env) {
      this.env = env;
      return this;
    }

    public Builder setCmd(String[] cmd) {
      this.cmd = cmd;
      return this;
    }

    public Builder setDns(String[] dns) {
      this.dns = dns;
      return this;
    }

    public Builder setImage(String image) {
      this.image = image;
      return this;
    }

    public Builder setVolumes(Map<String, ?> volumes) {
      this.volumes = volumes;
      return this;
    }

    public Builder setEntrypoint(String[] entrypoint) {
      this.entrypoint = entrypoint;
      return this;
    }

    public Builder setNetworkDisabled(boolean networkDisabled) {
      this.networkDisabled = networkDisabled;
      return this;
    }

    public Builder setPrivileged(boolean privileged) {
      this.privileged = privileged;
      return this;
    }

    public Builder setExposedPorts(Map<String, ?> exposedPorts) {
      this.exposedPorts = exposedPorts;
      return this;
    }

    public Builder setHostConfig(HostConfig hostConfig) {
      this.hostConfig = hostConfig;
      return this;
    }

    public Builder setWorkingDir(String workingDir) {
      this.workingDir = workingDir;
      return this;
    }

    public ContainerConfig build() {
      ContainerConfig config = new ContainerConfig(hostName, domainName, user, tty,
          stdinOpen, stdInOnce, attachStdin, attachStdout, attachStderr,
          env, cmd, image, volumes, entrypoint, networkDisabled,
          privileged, exposedPorts, hostConfig);
      config.setWorkingDir(workingDir);

      return config;
    }

  }
}
