/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * @author Frantisek Spacek
 */
public class ContainerInfo {

  @SerializedName("Id")
  private String id;

  @SerializedName("Warnings")
  private String[] warnings;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String[] getWarnings() {
    return warnings;
  }

  public void setWarnings(String[] warnings) {
    this.warnings = warnings;
  }

  @Override
  public String toString() {
    return "ContainerInfo{" + "id=" + id + ", warnings=" + Arrays.toString(warnings) + '}';
  }

}
