/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.docker.filter.impl;

import cz.utb.fai.apac.docker.filter.ListFilter;

public class ListFilterBuilder {

  private boolean all = false;
  private Integer limit = null;
  private String sinceId = null;
  private String beforeId = null;
  private boolean showSize = false;
  private String status = null;
  private Integer exitCode = null;

  public ListFilterBuilder() {
  }

  public ListFilterBuilder setAll(boolean all) {
    this.all = all;
    return this;
  }

  public ListFilterBuilder setLimit(Integer limit) {
    this.limit = limit;
    return this;
  }

  public ListFilterBuilder setSinceId(String sinceId) {
    this.sinceId = sinceId;
    return this;
  }

  public ListFilterBuilder setBeforeId(String beforeId) {
    this.beforeId = beforeId;
    return this;
  }

  public ListFilterBuilder setShowSize(boolean showSize) {
    this.showSize = showSize;
    return this;
  }

  public ListFilterBuilder setStatus(String status) {
    this.status = status;
    return this;
  }

  public ListFilterBuilder setExitCode(Integer exitCode) {
    this.exitCode = exitCode;
    return this;
  }

  public ListFilter build() {
    return new ListFilterImpl(all, limit, sinceId, beforeId,
        showSize, status, exitCode);
  }

}
