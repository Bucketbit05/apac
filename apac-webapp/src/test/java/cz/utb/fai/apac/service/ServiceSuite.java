/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author František Špaček
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({cz.utb.fai.apac.service.AssignmentServiceTest.class, cz.utb.fai.apac.service.VectorServiceTest.class, cz.utb.fai.apac.service.SyncServiceImplIntTest.class, cz.utb.fai.apac.service.SubmissionServiceTest.class})
public class ServiceSuite {

}
