/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.Application;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.repository.SubmissionRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import java.io.File;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

/**
 * @author František Špaček
 */
@Transactional
@ActiveProfiles("test")
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class WorkspaceCleanerTest {

  @Autowired
  private WorkspaceCleaner cleaner;

  @Autowired
  private SubmissionRepository submissionRepository;

  @Value("${apac.cleaner.days}")
  private int days;

  @Value("${apac.temp}")
  private String tempPath;

  @Test
  public void testClean() {
    cleaner.clean();
    List<Submission> submissions = submissionRepository.findAllBetween(LocalDateTime.of(1970, 1, 1, 0, 0), LocalDateTime.now().minusDays(days));

    for (Submission s : submissions) {
      Assert.isTrue(!s.isVisible());
      Assert.isTrue(!s.isRunnable());

      File workspace = WorkspaceUtil.workspace(tempPath, s.getUuid());
      Assert.isTrue(!workspace.exists());
    }
  }

  @Test
  public void testMapSerialization() throws JsonProcessingException {
    Map<String, Double> map = new HashMap<>();
    map.put("gbrrthtrtr", 1.4);
    map.put("gbrrt222htrtr", 1.4);

    System.out.println(new ObjectMapper().writeValueAsString(map));
  }

}
