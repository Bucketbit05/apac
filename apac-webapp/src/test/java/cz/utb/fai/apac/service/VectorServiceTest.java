/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cz.utb.fai.apac.AbstractTest;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.exception.ResourceNotFoundException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author František Špaček
 */
public class VectorServiceTest extends AbstractTest {

  @Autowired
  private VectorService vectorService;

  @Autowired
  private AssignmentService assignmentService;

  @Test
  public void testGet() {
    Vector fetched = vectorService.get(createRandomVector().getId());
    assertNotNull(fetched.getId());
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testGet_notFound() {
    vectorService.get(10);
  }

  @Test
  public void testUpdate() {
    Vector vector = createRandomVector();

    Argument arg = new Argument();
    arg.setName("-b");
    arg.setValue("10");
    arg.setType(ArgumentType.INTEGER);

    vector.getArguments().add(arg);
    vector = vectorService.update(vector);
    assertTrue(vector.getArguments().size() == 2);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testDelete() {
    Vector vector = createRandomVector();
    vectorService.delete(vector.getId());
    vectorService.get(vector.getId());
  }

  @Test
  public void testUpdateFile() {

  }

  @Test
  public void testUpdateRefOutput() {
    String refOutput = "REF_OUTPUT";
    Vector vector = createRandomVector();
    vectorService.updateRefOutput(vector, refOutput);

    Vector fetched = vectorService.get(vector.getId());
    Assert.assertEquals(fetched.getRefOutput(), refOutput);
  }

  private Vector createRandomVector() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());
    assertFalse(created.isVisible());

    Vector vector = TestUtil.randomVector(created);

    Argument arg = new Argument();
    arg.setName("-b");
    arg.setValue("10");
    arg.setType(ArgumentType.INTEGER);

    List<Argument> arguments = new ArrayList<>();
    arguments.add(arg);
    vector.setArguments(arguments);

    vector = assignmentService.addTestVector(created.getId(), vector);
    assertNotNull(vector.getId());

    return vector;
  }
}
