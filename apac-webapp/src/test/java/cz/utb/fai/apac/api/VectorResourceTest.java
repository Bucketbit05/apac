/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cz.utb.fai.apac.Application;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.api.dto.ArgumentDto;
import cz.utb.fai.apac.api.dto.VectorDto;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.repository.ArgumentRepository;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.VectorRepository;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author František Špaček
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Transactional
public class VectorResourceTest {

  @Autowired
  private WebApplicationContext context;

  @Autowired
  private AssignmentRepository assignmentRepositoryMock;

  @Autowired
  private VectorRepository vectorRepositoryMock;

  @Autowired
  private ArgumentRepository argumentRepositoryMock;

  private MockMvc mockMvc;

  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
  }

  @Test
  public void testGet() throws Exception {
    Vector vector = saveRandomVector();

    mockMvc.perform(get("/api/vectors/{id}", vector.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", is(vector.getId().intValue())));

  }

  @Test
  public void testUpdate() throws Exception {
    Vector vector = saveRandomVector();

    VectorDto dto = VectorDto.fromEntity(vector);
    dto.setPoints(30);

    mockMvc.perform(put("/api/vectors/{id}", vector.getId())
        .content(TestUtil.convertObjectToJsonBytes(dto))
        .contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", is(vector.getId().intValue())))
        .andExpect(jsonPath("$.points", is(30.0)));
  }

  @Test
  public void testDelete() throws Exception {
    Vector vector = saveRandomVector();

    mockMvc.perform(delete("/api/vectors/{id}", vector.getId()))
        .andExpect(status().isOk());

    Vector fetched = vectorRepositoryMock.findOne(vector.getId());
    Assert.isNull(fetched);
  }

  @Test
  public void testGetArguments() throws Exception {
    Vector vector = saveRandomVector();

    mockMvc.perform(get("/api/vectors/{id}/arguments", vector.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$[0].id", is(vector.getArguments().get(0).getId()
            .intValue())));
  }

  private Assignment saveRandomAssignment() {
    Assignment assignment = new Assignment();
    assignment.setName(RandomStringUtils.randomAlphabetic(200));
    assignment.setLang("C");
    assignment.setMaxRuntime(1);
    assignment.setMaxSize(1);
    assignment.setThreshold(50);
    assignment.setDescription(RandomStringUtils.randomAlphabetic(100));
    assignment.setAllowedCompileWarnings(1);
    assignment.setCompilationWeight(0.5);
    assignment.setExecutionWeight(0.5);
    assignmentRepositoryMock.save(assignment);

    return assignment;
  }

  private Vector saveRandomVector() {
    Assignment assignment = saveRandomAssignment();

    List<Argument> args = randomArguments().stream()
        .map(ArgumentDto::toEntity)
        .collect(Collectors.toList());

    Vector entity = new Vector();
    entity.setAssignment(assignment);
    entity.setMaxPoints(10);
    entity.setRefOutput(RandomStringUtils.randomAlphabetic(10));
    vectorRepositoryMock.save(entity);

    args.stream().forEach((a) -> {
      a.setVector(entity);
    });
    argumentRepositoryMock.save(args);

    entity.setArguments(args);
    vectorRepositoryMock.save(entity);

    assignment.getVectors().add(entity);
    assignmentRepositoryMock.save(assignment);

    return entity;
  }

  private List<ArgumentDto> randomArguments() {

    List<ArgumentDto> dtos = new ArrayList<>();

    ArgumentDto arg1 = new ArgumentDto();
    arg1.setName(RandomStringUtils.randomAlphabetic(1));
    arg1.setValue(RandomStringUtils.random(2));
    arg1.setType(ArgumentType.DOUBLE);
    dtos.add(arg1);

    ArgumentDto arg2 = new ArgumentDto();
    arg2.setName(RandomStringUtils.randomAlphabetic(1));
    arg2.setValue(RandomStringUtils.random(2));
    arg2.setType(ArgumentType.INPUTFILE);
    dtos.add(arg2);

    ArgumentDto arg3 = new ArgumentDto();
    arg3.setName(RandomStringUtils.randomAlphabetic(1));
    arg3.setValue(RandomStringUtils.random(2));
    arg3.setType(ArgumentType.STDINFILE);
    dtos.add(arg3);

    return dtos;

  }
}
