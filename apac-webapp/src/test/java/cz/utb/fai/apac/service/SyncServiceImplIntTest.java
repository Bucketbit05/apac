/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;

import cz.utb.fai.apac.TestApplication;
import cz.utb.fai.apac.api.dto.PlagiarismSyncDto;
import cz.utb.fai.apac.api.dto.SubmissionSimilarityDto;
import cz.utb.fai.apac.repository.SubmissionSimilarityRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author František Špaček
 */
@Sql("classpath:test-data.sql")
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebAppConfiguration
public class SyncServiceImplIntTest {

  private static final String BASE_UUID = "aea8afeaaf404bb29a220e0214cf5788";
  private static final String UUID1 = "5e790083-7cd3-4d0f-ae67-a13b690ea189";
  private static final String UUID2 = "8b559dc2-adce-410f-8729-5a8e5ceeea62";
  private static final String UUID3 = "b0cb5427-765f-4f7a-b38b-1a6698179054";
  private static final String UUID4 = "c7b5bf17-b928-4803-9420-08239aad62b9";

  @Autowired
  private SyncService syncService;

  @Autowired
  private SubmissionSimilarityRepository similarityRepository;

  @Test
  public void testSyncPlagiarism() {
    System.out.println("syncPlagiarism");
    PlagiarismSyncDto dto = new PlagiarismSyncDto();
    dto.setBaseUuid(BASE_UUID);
    dto.setSimilarity(1);
    dto.setSimilarSubmissions(asList(
        suspected(UUID1, 0.5),
        suspected(UUID1, 0.5),
        suspected(UUID3, 1),
        suspected(UUID4, 0.1)
    ));
    syncService.syncPlagiarism(asList(dto,dto));

    assertTrue(similarityRepository.count() > 0);

  }

  private SubmissionSimilarityDto suspected(String uuid, double similarity) {
    final SubmissionSimilarityDto dto = new SubmissionSimilarityDto();
    dto.setSimilarity(similarity);
    dto.setUuid(uuid);
    return dto;
  }
}
