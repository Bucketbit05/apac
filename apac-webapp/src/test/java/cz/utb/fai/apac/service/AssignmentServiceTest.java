/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.service;

import static org.junit.Assert.*;

import cz.utb.fai.apac.AbstractTest;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.api.dto.filter.AssignmentFilter;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.exception.ResourceNotFoundException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author František Špaček
 */
public class AssignmentServiceTest extends AbstractTest {

  @Autowired
  @InjectMocks
  private AssignmentService assignmentService;

  @Before
  public void initMocks() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testCreate() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());
    assertFalse(created.isVisible());
  }

  @Test
  public void testUpdate() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());
    assertFalse(created.isVisible());

    created.setDescription(RandomStringUtils.randomNumeric(10));
    created.setName(RandomStringUtils.randomNumeric(10));
    created.setAllowedCompileWarnings(RandomUtils.nextInt());
    created.setMaxRuntime(RandomUtils.nextInt());
    created.setMaxSize(RandomUtils.nextInt());
    created.setThreshold(RandomUtils.nextInt());
    created.setBasefile("/update/test.zip");
    created.setCreatedReference(Boolean.FALSE);
    created.setValidFrom(LocalDateTime.now());
    created.setValidTo(LocalDateTime.now().plusDays(5));

    Assignment updated = assignmentService.update(created);
    assertEquals(updated, created);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testDelete() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());

    assignmentService.delete(created.getId());
    assertNull(assignmentService.get(created.getId()));
  }

  @Test
  public void testGet() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());

    Assignment fetched = assignmentService.get(created.getId());
    assertEquals(created, fetched);
  }

  @Test
  public void testGetAll() {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());
    assertFalse(created.isVisible());

    created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());

    assignmentService.update(created);

    AssignmentFilter filter = new AssignmentFilter();
    List<Assignment> assignments = assignmentService.getAll(filter);
    assertFalse(assignments.isEmpty());
    assertTrue(assignments.size() == 1);

  }

  @Test
  public void testAddTestVector() throws IOException {
    Assignment created = assignmentService.create(TestUtil.randomAssignment());
    assertNotNull(created.getId());
    assertFalse(created.isVisible());

    Vector vector = TestUtil.randomVector(created);

    Argument arg = new Argument();
    arg.setName("-a");
    arg.setValue("10");
    arg.setType(ArgumentType.LONG);

    List<Argument> arguments = new ArrayList<>();
    arguments.add(arg);
    vector.setArguments(arguments);

    vector = assignmentService.addTestVector(created.getId(), vector);
    assertNotNull(vector.getId());
  }

  @Test
  public void testReRunAssignment() {
  }

}
