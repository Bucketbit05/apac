/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac;

import cz.utb.fai.apac.repository.ArgumentRepository;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.ExecutionRepository;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.repository.VectorRepository;

import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author František Špaček
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebAppConfiguration
public abstract class AbstractTest {

  @Autowired
  private AssignmentRepository assignmentRepository;

  @Autowired
  private SubmissionRepository submissionRepository;

  @Autowired
  private ExecutionRepository executionRepository;

  @Autowired
  private VectorRepository vectorRepository;

  @Autowired
  private ArgumentRepository argumentRepository;

  @After
  public void cleanUp() {
    argumentRepository.deleteAll();
    vectorRepository.deleteAll();
    executionRepository.deleteAll();
    submissionRepository.deleteAll();
    assignmentRepository.deleteAll();
  }
}
