/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cz.utb.fai.apac.Application;
import cz.utb.fai.apac.TestUtil;
import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.util.UUIDUtil;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

/**
 * @author František Špaček
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Transactional
public class SubmissionResourceTest {

  @Autowired
  private AssignmentRepository assignmentRepositoryMock;

  @Autowired
  private SubmissionRepository submissionRepositoryMock;

  @Autowired
  private WebApplicationContext context;

  private MockMvc mockMvc;

  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
  }

  @Test
  public void testGet() throws Exception {
    Submission submission = saveRandomSubmission();

    mockMvc.perform(get("/api/submissions/{uuid}", submission.getUuid()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.uuid", is(submission.getUuid())));

  }

  @Test
  public void testDelete() throws Exception {
    Submission submission = saveRandomSubmission();
    submissionRepositoryMock.save(submission);

    mockMvc.perform(delete("/api/submissions/{uuid}", submission.getUuid()))
        .andExpect(status().isOk());

    Optional<Submission> opFetched = submissionRepositoryMock.findByUuid(submission.getUuid());
    Assert.isTrue(!opFetched.isPresent());

  }

  @Test
  public void testGetAll() throws Exception {
    for (int i = 0; i < 3; i++) {
      saveRandomSubmission();
    }

    mockMvc.perform(post("/api/submissions/filter")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(TestUtil.convertObjectToJsonBytes(new SubmissionFilter())))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$[0].uuid", notNullValue()))
        .andExpect(jsonPath("$[1].uuid", notNullValue()))
        .andExpect(jsonPath("$[2].uuid", notNullValue()));
  }

  @Test
  public void testReRun() {
    //TODO
  }

  @Test
  public void testGetSubmissionExecutions() {
    //TODO
  }

  private Submission saveRandomSubmission() {
    Assignment assignment = saveRandomAssignment();
    Submission submission = new Submission();
    submission.setAssignment(assignment);
    submission.setCompileFailed(Boolean.FALSE);
    submission.setCompileOutput("");
    submission.setReference(Boolean.FALSE);
    submission.setTotalPoints(10);
    submission.setUuid(UUIDUtil.random());
    submissionRepositoryMock.save(submission);

    assignment.getSubmissions().add(submission);
    assignmentRepositoryMock.save(assignment);

    return submission;
  }

  private Assignment saveRandomAssignment() {
    Assignment assignment = new Assignment();
    assignment.setName(RandomStringUtils.randomAlphabetic(200));
    assignment.setLang("C");
    assignment.setMaxRuntime(1);
    assignment.setMaxSize(1);
    assignment.setThreshold(50);
    assignment.setDescription(RandomStringUtils.randomAlphabetic(100));
    assignment.setAllowedCompileWarnings(1);
    assignment.setCompilationWeight(0.5);
    assignment.setExecutionWeight(0.5);
    assignmentRepositoryMock.save(assignment);

    return assignment;
  }
}
