/* 
 * Copyright (C) 2016 František Špaček
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Author:  František Špaček
 * Created: Jan 13, 2016
 */
INSERT INTO assignments(id, visible, allowed_compile_warnings, assignment_plag_id, basefile, 
            compilation_weight, created_reference, description, enable_networking, 
            execution_weight, lang, max_runtime, max_size, name, threshold, 
            valid_from, valid_to)
VALUES  (1,true, 0, NULL,'', 0.5, true, 'test', true, 0.5, 'C', 30, 1, 'test',60, now(), now());

INSERT INTO submissions(
            id, visible, compile_failed, compile_output, owner, plagiarism_checked, 
            reference, runnable, similarity, time_stamp, total_points, uuid, 
            assignment_id) VALUES 
    (1, true, false, '', 'a', false, false, true, 0, now(), 10, 'aea8afeaaf404bb29a220e0214cf5788', 1),
    (2, true, false, '', 'b', false, false, true, 0, now(), 10, '5e7900837cd34d0fae67a13b690ea189', 1),
    (3, true, false, '', 'c', false, false, true, 0, now(), 10, '8b559dc2adce410f87295a8e5ceeea62', 1),
    (4, true, false, '', 'd', false, false, true, 0, now(), 10, 'b0cb5427765f4f7ab38b1a6698179054', 1);

