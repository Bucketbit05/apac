/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * @author František Špaček
 */
public class WorkspaceUtil {

  public static String workspacePath(String basePath, String workspaceUUID) {
    String workspacePath = basePath;

    if (!workspacePath.endsWith("/")) {
      workspacePath += File.separator;
    }
    workspacePath += workspaceUUID;

    return workspacePath;
  }

  public static File workspace(String basePath, String workspaceUUID) {
    String workspacePath = basePath;

    if (!workspacePath.endsWith("/")) {
      workspacePath += File.separator;
    }
    workspacePath += workspaceUUID;

    return new File(workspacePath);
  }

  public static void deleteWorkspace(String basePath, String workspaceUUID) {
    FileUtils.deleteQuietly(workspace(basePath, workspaceUUID));
  }
}
