/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import cz.utb.fai.apac.api.dto.ExecutionDto;
import cz.utb.fai.apac.api.dto.SubmissionDto;
import cz.utb.fai.apac.api.dto.filter.SubmissionFilter;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.service.SubmissionService;
import cz.utb.fai.apac.util.UploadUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author František Špaček
 */
@RestController
@Api(value = "Submissions", description = "Endpoint for submissions management")
@RequestMapping("/api/submissions")
public class SubmissionResource extends CrudResource<SubmissionDto, String> {

  @Autowired
  private SubmissionService submissionService;

  @Value("${apac.uploads}")
  private String uploadPath;

  @Override
  @ApiOperation(value = "Get submission")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission found"),
    @ApiResponse(code = 404, message = "Submission not found")
  })
  @RequestMapping(value = "{uuid}", method = RequestMethod.GET)
  public SubmissionDto get(
      @ApiParam(required = true, name = "uuid", value = "Submission uuid")
      @PathVariable("uuid") String uuid) {
    return SubmissionDto.fromEntity(submissionService.get(uuid));
  }

  @Override
  @ApiOperation(value = "Delete submission")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission found and deleted"),
    @ApiResponse(code = 404, message = "Submission not found")
  })
  @RequestMapping(value = "{uuid}", method = RequestMethod.DELETE)
  public void delete(
      @ApiParam(required = true, name = "uuid", value = "Submission uuid")
      @PathVariable("uuid") String uuid) {
    submissionService.delete(uuid);
  }

  @ApiOperation(value = "Submission filter")
  @ApiResponses({
    @ApiResponse(code = 200, message = "List of filtered submissions")
  })
  @RequestMapping(value = "/filter", method = RequestMethod.POST)
  public List<SubmissionDto> getAll(
      @RequestBody(required = false) SubmissionFilter filter) {
    List<Submission> submissions = submissionService.getAll(filter);
    return submissions.stream()
        .map(SubmissionDto::fromEntity)
        .collect(Collectors.toList());
  }

  @ApiOperation(value = "Rerun submission")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission added to processing queue"),
    @ApiResponse(code = 404, message = "Submission not found")
  })
  @RequestMapping(value = "{uuid}", method = RequestMethod.PUT)
  public void reRun(
      @ApiParam(required = true, name = "uuid", value = "Submission uuid")
      @PathVariable("uuid") String uuid) {
    submissionService.reRunSubmission(uuid);
  }

  @ApiOperation(value = "Reupload submission")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission added to processing queue"),
    @ApiResponse(code = 404, message = "Submission not found")
  })
  @RequestMapping(value = "{uuid}/upload", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public SubmissionDto reUpload(
      @ApiParam(required = true, name = "uuid", value = "Submission uuid")
      @PathVariable("uuid") String uuid,
      @NotNull HttpEntity<byte[]> file,
      @ApiParam(required = false, name = "filename",
          value = "Submission filename")
      @NotNull @Size(min = 1) @RequestParam(value = "filename",
          required = false) String filename) throws IOException {

    //validate mediaType and copy to uploadPath
    File uploadedFile = UploadUtil
        .processUpload(file.getBody(), uploadPath, filename.isEmpty()
            ? String.valueOf(System.currentTimeMillis())
            : filename);

    //process submission
    submissionService.processSubmission(uuid, uploadedFile);

    return new SubmissionDto(uuid);
  }

  @ApiOperation(value = "Submission executions")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Submission executions"),
    @ApiResponse(code = 404, message = "Submission not found")
  })
  @RequestMapping(value = "/{uuid}/executions", method = RequestMethod.GET)
  public List<ExecutionDto> getSubmissionExecutions(
      @ApiParam(required = true, name = "uuid", value = "Submission uuid")
      @PathVariable("uuid") String uuid) {
    Submission submission = submissionService.get(uuid);
    return submission.getExecutions().stream()
        .map(ExecutionDto::fromEntity)
        .collect(Collectors.toList());
  }

}
