/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.service.impl;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.slf4j.LoggerFactory.getLogger;

import cz.utb.fai.apac.api.dto.filter.AssignmentFilter;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Assignment_;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import cz.utb.fai.apac.exception.InvalidVectorException;
import cz.utb.fai.apac.exception.ResourceNotFoundException;
import cz.utb.fai.apac.repository.ArgumentRepository;
import cz.utb.fai.apac.repository.AssignmentRepository;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.repository.VectorRepository;
import cz.utb.fai.apac.service.AssignmentService;
import cz.utb.fai.apac.service.SubmissionService;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author František Špaček
 */
@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

  private static final Logger LOG = getLogger(AssignmentServiceImpl.class);
  @Autowired
  private AssignmentRepository assignmentRepository;
  @Autowired
  private SubmissionRepository submissionRepository;
  @Autowired
  private VectorRepository vectorRepository;
  @Autowired
  private ArgumentRepository argumentRepository;
  @Autowired
  private SubmissionService submissionService;

  @Override
  public Assignment create(Assignment entity) {

    //new assignment is not visible until all steps are done
    entity.setVisible(false);
    entity = assignmentRepository.save(entity);

    return entity;
  }

  @Override
  public Assignment update(Assignment entity) {
    Assignment assignment = get(entity.getId());
    //merge saved assignment with updated
    entity.setBasefile(assignment.getBasefile());
    entity.setCreatedReference(assignment.isCreatedReference());
    entity.setVectors(assignment.getVectors());
    entity.setSubmissions(assignment.getSubmissions());
    entity.setVisible(
        !StringUtils.isEmpty(assignment.getBasefile())
        && assignment.isCreatedReference());
    return assignmentRepository.save(entity);
  }

  @Override
  public Assignment internalUpdate(Assignment entity) {
    Assignment assignment = get(entity.getId());
    //merge saved assignment with updated
    entity.setBasefile(assignment.getBasefile());
    entity.setCreatedReference(entity.isCreatedReference());
    entity.setVectors(assignment.getVectors());
    entity.setSubmissions(assignment.getSubmissions());
    entity.setVisible(entity.isCreatedReference());
    return assignmentRepository.save(entity);
  }

  @Override
  public void delete(long id) {
    Assignment assignment = get(id);

    if (assignment.getSubmissions().isEmpty()) {
      assignmentRepository.delete(id);
    } else {
      assignment.setVisible(false);

      List<Submission> submissions = assignment.getSubmissions();
      submissions.stream().forEach((s) -> {
        s.setVisible(false);
        s.setRunnable(false);
      });

      submissionRepository.save(submissions);
      assignmentRepository.save(assignment);
    }
  }

  @Override
  public Assignment get(long id) {
    Optional<Assignment> opAssignment = assignmentRepository.findById(id);
    return opAssignment.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public List<Assignment> getAll(AssignmentFilter filter) {
    return assignmentRepository
        .findAll(FilterSpec.searchByFilter(filter == null
            ? new AssignmentFilter()
            : filter));
  }

  @Override
  public Vector addTestVector(long id, Vector vector) {
    Assignment assignment = get(id);

    Optional<Argument> opVectorFile = vector.getArguments()
        .stream()
        .filter(a -> a.getType().equals(ArgumentType.INPUTFILE)
            || a.getType().equals(ArgumentType.STDINFILE))
        .findFirst();

    if (opVectorFile.isPresent()) {

      final String vectorFileName = opVectorFile.get().getName();

      if (!StringUtils.isEmpty(assignment.getBasefile())) {
        File referenceDirectory = new File(assignment.getBasefile());

        if (!checkIfVectorFileIsPresented(referenceDirectory, vectorFileName)) {
          throw new InvalidVectorException(format("File %s is not presented in reference directory."
              + " First upload zip with all required files, than define vectors for these files",
              vectorFileName));
        }
      }
      vector.setFilename(vectorFileName);
    }

    vector.setAssignment(assignment);

    assignment.getVectors().add(vector);
    vectorRepository.save(vector);

    vector.getArguments().forEach(a -> {
      a.setVector(vector);
    });
    argumentRepository.save(vector.getArguments());

    reRunAssignment(id);

    return vector;
  }

  @Override
  public void reRunAssignment(long id) {
    Assignment assignment = get(id);
    if (!StringUtils.isEmpty(assignment.getBasefile())) {

      try {
        submissionService.reRunReferenceSubmission(assignment);
        List<Submission> submissions = submissionService.getAllCurrent(assignment);

        //test already executed submissions
        if (!submissions.isEmpty()) {
          submissions.forEach(s -> {
            submissionService.reRunSubmission(s.getUuid());
          });
        }
      } catch (IOException ex) {
        LOG.error("Error during add vector to assignment ", ex);
      }
    }
  }

  private boolean checkIfVectorFileIsPresented(File referenceDirectory, String name) {
    return asList(referenceDirectory.listFiles())
        .stream().filter(f -> f.getName().equals(name))
        .findFirst().isPresent();
  }

  private static class FilterSpec {

    public static Specification<Assignment> searchByFilter(
        AssignmentFilter filter) {
      return (Root<Assignment> root, CriteriaQuery<?> query,
          CriteriaBuilder cb) -> {
        Predicate predicate = cb.conjunction();

        predicate.getExpressions()
            .add(cb.equal(root.get(Assignment_.visible), filter.isVisible()));

        if (!StringUtils.isEmpty(filter.getLanguage())) {
          predicate.getExpressions()
              .add(cb.equal(root.get(Assignment_.language),
                  filter.getLanguage()));
        }

        return predicate;
      };
    }

  }
}
