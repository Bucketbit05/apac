/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Data transfer object for synchronization of result from PlagDetector application.
 *
 * @author František Špaček
 */
public class PlagiarismSyncDto {

  @NotNull
  private String baseUuid;
  private double similarity = 0d;
  private List<SubmissionSimilarityDto> similarSubmissions = new ArrayList<>();

  public String getBaseUuid() {
    return baseUuid;
  }

  public void setBaseUuid(String baseUuid) {
    this.baseUuid = baseUuid;
  }

  public double getSimilarity() {
    return similarity;
  }

  public void setSimilarity(double similarity) {
    this.similarity = similarity;
  }

  public List<SubmissionSimilarityDto> getSimilarSubmissions() {
    return similarSubmissions;
  }

  public void setSimilarSubmissions(List<SubmissionSimilarityDto> similarSubmissions) {
    this.similarSubmissions = similarSubmissions;
  }

  @Override
  public String toString() {
    return "PlagiarismSyncDTO{" + "baseUuid=" + baseUuid + ", similarity=" + similarity
        + ", similarSubmissions=" + similarSubmissions + '}';
  }
}
