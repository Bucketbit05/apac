
package cz.utb.fai.apac.core;

import static java.util.Arrays.asList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.config.STOMPConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author František Špaček
 */
@Component
public class SubmissionMonitor {

  private static final String INFO = "INFO";
  private static final String ERROR = "ERROR";
  private static final String START = "##START##";
  private static final String END = "##END##";
  private static final Logger LOG = LoggerFactory.getLogger(SubmissionMonitor.class);

  @Autowired
  private SimpMessagingTemplate msgTemplate;

  public void sendInfo(String uuid, String msg) {
    List<MonitorMessage> messages = new ArrayList<>();
    messages.add(new MonitorMessage(INFO, msg));
    send(uuid, messages);
  }

  public void sendLogs(String uuid, InputStream is) {
    Scanner scanner = new Scanner(is);
    while (scanner.hasNextLine()) {
      send(uuid, asList(new MonitorMessage(INFO, scanner.nextLine())));
    }
  }

  public void sendError(String uuid, String msg) {
    List<MonitorMessage> messages = new ArrayList<>();
    messages.add(new MonitorMessage(ERROR, msg));
    send(uuid, messages);

  }

  public void sendStart(String uuid) {
    msgTemplate.convertAndSendToUser(uuid, STOMPConfig.STOMP_ENDPOINT, START);
  }

  public void sendEnd(String uuid) {
    msgTemplate.convertAndSendToUser(uuid, STOMPConfig.STOMP_ENDPOINT, END);
  }

  private void send(String uuid, List<MonitorMessage> messages) {
    msgTemplate.convertAndSendToUser(uuid, STOMPConfig.STOMP_ENDPOINT, toJson(messages));
  }

  private String toJson(List<MonitorMessage> messages) {
    String json = "";
    try {
      json = new ObjectMapper().writeValueAsString(messages);
    } catch (JsonProcessingException ex) {
      LOG.error("Error during json processing", ex);
    }

    return json;
  }

  private class MonitorMessage {

    private String level;
    private String message;

    public MonitorMessage() {
    }

    public MonitorMessage(String level, String message) {
      this.level = level;
      this.message = message;
    }

    public String getLevel() {
      return level;
    }

    public void setLevel(String level) {
      this.level = level;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    @Override
    public String toString() {
      return "MonitorMessage{" + "level="
          + level + ", message="
          + message + '}';
    }
  }
}
