/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.monitoring;

import cz.utb.fai.apac.docker.client.DockerClient;
import cz.utb.fai.apac.docker.exception.DockerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

/**
 *
 * @author František Špaček
 */
@Component
public class DockerHealthIndicator extends AbstractHealthIndicator {

  private final DockerClient client;

  @Autowired
  public DockerHealthIndicator(DockerClient client) {
    this.client = client;
  }

  @Override
  protected void doHealthCheck(Health.Builder builder) throws Exception {
    try {
      builder
          .withDetail("version", client.version())
          .withDetail("info", client.info())
          .up();
    } catch (DockerException ex) {
      builder.down(ex);
    }

  }

}
