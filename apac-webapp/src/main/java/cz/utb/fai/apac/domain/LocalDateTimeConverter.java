/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author František Špaček
 */
/**
 * Converts {@link Timestamp} to {@link LocalDateTime} and back.
 */
@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {
  // mapping with java.util.Calendar breaks EclipseLink

  @Override
  public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {
    if (attribute == null) {
      return null;
    }

    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    calendar.set(Calendar.YEAR, attribute.getYear());
    // avoid 0 vs 1 based months
    calendar.set(Calendar.DAY_OF_YEAR, attribute.getDayOfYear());
    calendar.set(Calendar.HOUR_OF_DAY, attribute.getHour());
    calendar.set(Calendar.MINUTE, attribute.getMinute());
    calendar.set(Calendar.SECOND, attribute.getSecond());
    calendar.set(Calendar.MILLISECOND, (int) (attribute.getNano() / 1000000L));
    return new Timestamp(calendar.getTimeInMillis());
  }

  @Override
  public LocalDateTime convertToEntityAttribute(Timestamp dbData) {
    if (dbData == null) {
      return null;
    }

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dbData);
    // convert 0 vs 1 based months
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minute = calendar.get(Calendar.MINUTE);
    int second = calendar.get(Calendar.SECOND);
    int nanoOfSecond = calendar.get(Calendar.MILLISECOND) * 1000000;
    return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoOfSecond);
  }
}
