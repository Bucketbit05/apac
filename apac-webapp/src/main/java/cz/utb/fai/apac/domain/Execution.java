/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * @author František Špaček
 */
@Entity
@Table(name = "executions")
public class Execution extends BaseEntity {

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "submission_id")
  private Submission submission;

  @Column(nullable = false)
  private double score = 0.0d;

  @Type(type = "text")
  private String log;

  @Column(nullable = false)
  private Boolean terminated = false;

  private Integer statusCode;

  @Type(type = "serializable")
  private Vector vector;

  public Submission getSubmission() {
    return submission;
  }

  public void setSubmission(Submission submission) {
    this.submission = submission;
  }

  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public String getLog() {
    return log;
  }

  public void setLog(String log) {
    this.log = log;
  }

  public Boolean getTerminated() {
    return terminated;
  }

  public void setTerminated(Boolean terminated) {
    this.terminated = terminated;
  }

  public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public Vector getVector() {
    return vector;
  }

  public void setVector(Vector vector) {
    this.vector = vector;
  }

  @Override
  public String toString() {
    return "Execution{" + "score=" + score + ", log=" + log + ", terminated=" + terminated + ", statusCode=" + statusCode + '}';
  }
}
