/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import com.fasterxml.jackson.core.JsonProcessingException;

import cz.utb.fai.apac.docker.client.DockerClient;
import cz.utb.fai.apac.docker.exception.DockerException;
import cz.utb.fai.apac.docker.exception.DockerNotRunningException;
import cz.utb.fai.apac.docker.filter.LogsFilter;
import cz.utb.fai.apac.docker.filter.impl.LogsFilterBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Reader for logs from docker container.
 *
 * @author František Špaček
 */
@Component
public class ExecutorLogReader {

  @Autowired
  private SubmissionMonitor monitor;

  private static final Logger LOG = LoggerFactory.getLogger(ExecutorLogReader.class);

  private final LogsFilter filter = new LogsFilterBuilder()
      .setFollow(true)
      .setStderr(true)
      .setStdout(true)
      .build();

  @Async
  public void readAsyncLogs(DockerClient client, String workspaceUUID, String execContainerId)
      throws DockerException, DockerNotRunningException, JsonProcessingException {
    LOG.debug("Reading logs from container");
    monitor.sendLogs(workspaceUUID, client.getLogs(execContainerId, filter));
  }

  public void readSyncLogs(DockerClient client, String workspaceUUID, String execContainerId)
      throws DockerException, DockerNotRunningException, JsonProcessingException {
    monitor.sendLogs(workspaceUUID, client.getLogs(execContainerId, filter));
  }

  public void sendErrorLog(String workspaceUUID, String log) {
    monitor.sendError(workspaceUUID, log);
  }

}
