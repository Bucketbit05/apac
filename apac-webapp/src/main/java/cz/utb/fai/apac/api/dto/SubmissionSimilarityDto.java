/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import cz.utb.fai.apac.domain.SubmissionSimilarity;

/**
 *
 * @author František Špaček
 */
public class SubmissionSimilarityDto {

  private String uuid;
  private double similarity = 0d;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public double getSimilarity() {
    return similarity;
  }

  public void setSimilarity(double similarity) {
    this.similarity = similarity;
  }

  public static SubmissionSimilarityDto fromEntity(SubmissionSimilarity entity) {
    SubmissionSimilarityDto dto = new SubmissionSimilarityDto();
    dto.setSimilarity(entity.getSimilarity());
    dto.setUuid(entity.getSuspected().getUuid());
    return dto;
  }

  @Override
  public String toString() {
    return "SubmissionSimilarity{" + "uuid=" + uuid + ", similarity=" + similarity + '}';
  }
}
