/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import cz.utb.fai.apac.api.dto.PlagiarismSyncDto;
import cz.utb.fai.apac.service.SyncService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author František Špaček
 */
@RestController
@Api(value = "Sync", description = "Endpoint for plagiarism synchronization")
@RequestMapping("/sync")
public class SyncResource {

  @Autowired
  private SyncService syncService;

  @ApiOperation(value = "Synchronize plagiarism results")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Synchronization successful")
  })
  @RequestMapping(value = "/plagiarism", method = RequestMethod.POST)
  public void updateSimilarity(@RequestBody List<PlagiarismSyncDto> dto) {
    syncService.syncPlagiarism(dto);
  }
}
