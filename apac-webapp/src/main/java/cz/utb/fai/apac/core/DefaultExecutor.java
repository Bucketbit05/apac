/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import static cz.utb.fai.apac.util.CommandUtil.buildArguments;
import static cz.utb.fai.apac.util.WorkspaceUtil.*;
import static java.util.Arrays.asList;
import static org.apache.commons.io.FileUtils.readFileToString;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.commons.CompileResult;
import cz.utb.fai.apac.commons.ExecutionResult;
import cz.utb.fai.apac.commons.enums.Extension;
import cz.utb.fai.apac.commons.spi.Processor;
import cz.utb.fai.apac.docker.client.DockerClient;
import cz.utb.fai.apac.docker.exception.DockerException;
import cz.utb.fai.apac.docker.exception.DockerNotRunningException;
import cz.utb.fai.apac.docker.filter.LogsFilter;
import cz.utb.fai.apac.docker.filter.impl.ListFilterBuilder;
import cz.utb.fai.apac.docker.filter.impl.LogsFilterBuilder;
import cz.utb.fai.apac.docker.model.Container;
import cz.utb.fai.apac.docker.model.ContainerConfig;
import cz.utb.fai.apac.docker.model.HostConfig;
import cz.utb.fai.apac.docker.model.StartConfig;
import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.domain.Vector;
import cz.utb.fai.apac.enums.ArgumentType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author František Špaček
 */
@Component("container-executor")
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultExecutor implements Executor {

  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultExecutor.class);

  private static final String NO_SOURCE_ERROR = "Incorrect format of submission";
  private static final String WORK_DIR = "/data";
  private static final String[] BASH_CMD = new String[]{"/bin/bash", "-c"};
  private final DockerClient client;
  private final ArrayList<String> containerIds = new ArrayList<>();
  private final LogsFilter filter = new LogsFilterBuilder()
      .setFollow(true)
      .setStderr(true)
      .setStdout(true)
      .build();

  @Autowired
  private ExecutorLogReader logReader;

  @Value("${apac.docker.image}")
  private String image;

  @Value("${apac.docker.cpu}")
  private Integer maxCPU;

  @Value("${apac.docker.memory}")
  private Integer maxMemory;

  @Value("${apac.temp}")
  private String basePath;

  @Autowired
  public DefaultExecutor(DockerClient client) {
    this.client = client;
  }

  @Override
  public CompileResult compile(String workspaceUUID, final Processor processor) {
    String compileOutput = NO_SOURCE_ERROR;

    String compileCommand = processor.compileCommand(
        workspace(basePath, workspaceUUID));

    if (!compileCommand.isEmpty()) {
      try {
        LOG.info("Compile command for {} is {}",
            workspaceUUID, compileCommand);

        ContainerConfig config = createConfig(workspaceUUID, compileCommand,
            false);

        LOG.debug("Compile container config {}",
            new ObjectMapper().writeValueAsString(config));

        String compileContainerId = client.create(config).getId();

        startContainer(workspaceUUID, compileContainerId);

        //read log stream as blocking
        logReader.readSyncLogs(client, workspaceUUID, compileContainerId);

        InputStream logStream = client.getLogs(compileContainerId, filter);
        compileOutput = readOutput(logStream);

      } catch (DockerException | DockerNotRunningException | IOException ex) {
        LOG.error("Compile execution error for workspace {}", workspaceUUID, ex);
      }
    } else {
      logReader.sendErrorLog(workspaceUUID, compileOutput);
    }

    CompileResult compileResult = new CompileResult(compileOutput);

    boolean failed = !processor.compilationSuccess(compileResult)
        || compileCommand.isEmpty();

    compileResult.setFailed(failed);

    LOG.info("Compilation for submission {} has been {}", workspaceUUID,
        compileResult.isFailed() ? "unsuccessful" : "successful");

    return compileResult;
  }

  @Override
  public ExecutionResult execute(String workspaceUUID,
      final Processor processor, Vector vector, int maxRuntime) {
    ExecutionResult exeResult = new ExecutionResult();
    exeResult.setTestVector(vector.getRefOutput(), vector.getMaxPoints());
    String workspacePath = workspacePath(basePath, workspaceUUID);
    try {
      //prepare result object
      String exeCommand = processor.executionCommand(new File(workspacePath))
          + " %s";
      exeCommand = String.format(exeCommand,
          buildArguments(vector, workspacePath));
      LOG.info("Executing submission with command {}", exeCommand);

      ContainerConfig config = createConfig(workspaceUUID, exeCommand, true);
      LOG.debug("Execution container config {}",
          new ObjectMapper().writeValueAsString(config));

      String execContainerId = client.create(config).getId();
      LOG.debug("Execution container id {}", execContainerId);

      startContainer(workspaceUUID, execContainerId);

      //read log stream as non blocking
      logReader.readAsyncLogs(client, workspaceUUID, execContainerId);

      boolean jobEnded = false;
      long startTime = System.currentTimeMillis();
      long endTime = System.currentTimeMillis();
      while ((endTime - startTime) < (maxRuntime * 1000)) {

        Optional<Container> exists = client.get(new ListFilterBuilder().build())
            .stream()
            .filter(c -> c.getId().equals(execContainerId))
            .findFirst();

        if (!exists.isPresent()) {
          LOG.info("Container {} for workspace UUID {} ended job",
              execContainerId, workspaceUUID);
          jobEnded = true;
          break;
        } else {
          LOG.debug("Container {} for workspace UUID {} still processing job",
              execContainerId, workspaceUUID);
        }

        endTime = System.currentTimeMillis();
      }

      if (!jobEnded) {
        client.kill(execContainerId);
        exeResult.setTerminated(true);

        LOG.info("Container {} for workspace UUID {} has been killed",
            execContainerId, workspaceUUID);
      } else {
        Optional<Argument> argument = vector.getArguments().stream()
            .filter(a -> a.getType().equals(ArgumentType.OUTPUTFILE)).findFirst();

        if (!argument.isPresent()) {
          InputStream logStream = client.getLogs(execContainerId, filter);
          exeResult.setOutput(readOutput(logStream));
        } else {
          File workspace = new File(workspacePath);

          File[] outputFiles = workspace
              .listFiles((File dir, String name)
                  -> name.endsWith(Extension.OUT.toString()));

          if (outputFiles.length > 0) {
            String outputFileContent = readFileToString(outputFiles[0]);
            exeResult.setOutput(outputFileContent);

          }
        }
        exeResult.setDuration(endTime - startTime);
        LOG.info("Execution complete, duration {} ms", endTime - startTime);
      }

    } catch (DockerException | DockerNotRunningException | IOException ex) {
      LOG.error("Execution error for workspace {}", workspaceUUID, ex);
    }
    return exeResult;
  }

  @Override
  public void cleanUp(String workspaceUUID) {
    try {
      for (String id : containerIds) {
        LOG.debug("Deleting container {}", id);
        client.delete(id);
      }
    } catch (DockerException ex) {
      LOG.error("CleanUp execution error for workspace {}", workspaceUUID, ex);
    }
  }

  private ContainerConfig createConfig(String workspaceUUID,
      String command, boolean exeContainer) {
    HashMap<String, ?> volumeMap = new HashMap<>();
    volumeMap.put(WORK_DIR, null);
    ContainerConfig config = new ContainerConfig.Builder()
        .setImage(image)
        .setHostName(workspaceUUID)
        .setTty(true)
        .setNetworkDisabled(true)
        .setVolumes(volumeMap)
        .setWorkingDir(WORK_DIR)
        .build();

    if (exeContainer) {
      HostConfig hostConfig = new HostConfig.Builder()
          .setMemoryLimit(maxMemory)
          .setMemorySwap(maxMemory)
          .setCpuShares(maxCPU)
          .build();
      config.setHostConfig(hostConfig);
    }

    List<String> cmdWithArgs = new ArrayList<>(asList(BASH_CMD));
    cmdWithArgs.add(command);

    String[] cmd = new String[3];
    config.setCmd(cmdWithArgs.toArray(cmd));

    return config;
  }

  private void startContainer(String workspaceUUID, String containerId)
      throws JsonProcessingException, DockerNotRunningException,
      DockerException {
    StartConfig startConfig = new StartConfig();
    startConfig.addBind(workspacePath(basePath, workspaceUUID), WORK_DIR, true);
    LOG.debug("Container start config {}",
        new ObjectMapper().writeValueAsString(startConfig));

    client.start(containerId, startConfig);
    containerIds.add(containerId);
  }

  private String readOutput(InputStream logStream) {
    StringBuilder outputBuilder = new StringBuilder();

    Scanner scanner = new Scanner(logStream);
    while (scanner.hasNextLine()) {
      outputBuilder.append(scanner.nextLine());
      outputBuilder.append(System.getProperty("line.separator"));
    }
    return outputBuilder.toString();
  }

}
