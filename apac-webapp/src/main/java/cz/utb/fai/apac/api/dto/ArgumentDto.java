/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import cz.utb.fai.apac.domain.Argument;
import cz.utb.fai.apac.enums.ArgumentType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author František Špaček
 */
public class ArgumentDto extends BaseDto {

  private String name = "";

  @NotNull
  private ArgumentType type;

  @NotNull
  @Size(min = 1)
  private String value;

  public static ArgumentDto fromEntity(Argument entity) {
    ArgumentDto dto = new ArgumentDto();
    dto.setId(entity.getId());

    dto.setName(entity.getName());
    dto.setType(entity.getType());
    dto.setValue(entity.getValue());
    return dto;
  }

  public static Argument toEntity(ArgumentDto dto) {
    Argument entity = new Argument();
    entity.setId(entity.getId());
    entity.setName(dto.getName());
    entity.setType(dto.getType());
    entity.setValue(dto.getValue());
    return entity;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArgumentType getType() {
    return type;
  }

  public void setType(ArgumentType type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "ArgumentDTO{" + "name=" + name + ", type=" + type + ", value=" + value + '}';
  }

}
