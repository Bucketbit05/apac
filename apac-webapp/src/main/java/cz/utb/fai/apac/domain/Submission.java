/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import org.hibernate.annotations.Type;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

/**
 * @author František Špaček
 */
@Entity
@Table(name = "submissions")
public class Submission extends BaseEntity {

  @Column(nullable = false)
  private String uuid;

  @Column(nullable = false)
  private double totalPoints = 0.0d;

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "assignment_id")
  private Assignment assignment;
  @Type(type = "text")

  private String compileOutput;
  @Column(nullable = false)

  private Boolean compileFailed = false;

  private Boolean reference = false;

  @Column(nullable = false, name = "time_stamp")
  private LocalDateTime timestamp = LocalDateTime.now();

  @Column(nullable = false)
  private Double similarity = 0d;

  @Column(nullable = false)
  private boolean runnable = true;

  @Column(nullable = false)
  private String owner = "";

  @Column(nullable = false)
  private boolean plagiarismChecked = false;

  @OneToMany(mappedBy = "base", targetEntity = SubmissionSimilarity.class)
  private Set<SubmissionSimilarity> similarSubmissions = new LinkedHashSet<>();

  @OneToMany(mappedBy = "submission", orphanRemoval = true)
  List<Execution> executions = new ArrayList<>();

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public double getTotalPoints() {
    return totalPoints;
  }

  public void setTotalPoints(double totalPoints) {
    this.totalPoints = totalPoints;
  }

  public Assignment getAssignment() {
    return assignment;
  }

  public void setAssignment(Assignment assignment) {
    this.assignment = assignment;
  }

  public String getCompileOutput() {
    return compileOutput;
  }

  public void setCompileOutput(String compileOutput) {
    this.compileOutput = compileOutput;
  }

  public Boolean getCompileFailed() {
    return compileFailed;
  }

  public void setCompileFailed(Boolean compileFailed) {
    this.compileFailed = compileFailed;
  }

  public Boolean getReference() {
    return reference;
  }

  public void setReference(Boolean reference) {
    this.reference = reference;
  }

  public List<Execution> getExecutions() {
    return executions;
  }

  public void setExecutions(List<Execution> executions) {
    this.executions = executions;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public Double getSimilarity() {
    return similarity;
  }

  public void setSimilarity(Double similarity) {
    this.similarity = similarity;
  }

  public boolean isRunnable() {
    return runnable;
  }

  public void setRunnable(boolean runnable) {
    this.runnable = runnable;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public boolean isPlagiarismChecked() {
    return plagiarismChecked;
  }

  public void setPlagiarismChecked(boolean plagiarismChecked) {
    this.plagiarismChecked = plagiarismChecked;
  }

  public Set<SubmissionSimilarity> getSimilarSubmissions() {
    return similarSubmissions;
  }

  public void setSimilarSubmissions(Set<SubmissionSimilarity> similarSubmissions) {
    this.similarSubmissions = similarSubmissions;
  }

  public void addSimilarSubmission(Submission submission, double similarity) {
    this.similarSubmissions.add(new SubmissionSimilarity(this, submission, similarity));
  }

  @Override
  public String toString() {
    return "Submission{" + "uuid=" + uuid + ", totalPoints=" + totalPoints + ", compileOutput="
        + compileOutput + ", compileFailed=" + compileFailed + ", reference=" + reference
        + ", timestamp=" + timestamp + ", similarity=" + similarity + ", runnable=" + runnable
        + ", owner=" + owner + '}';
  }

}
