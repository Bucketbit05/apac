/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import org.hibernate.annotations.Type;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * @author František Špaček
 */
@Entity
@Table(name = "vectors")
public class Vector extends BaseEntity {

  @OneToMany(mappedBy = "vector", targetEntity = Argument.class, fetch = FetchType.EAGER)
  private List<Argument> arguments = new ArrayList<>();

  @Type(type = "text")
  private String refOutput;

  @Column(nullable = false)
  private double maxPoints = 0.0d;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  private Assignment assignment;

  private String filename;

  public List<Argument> getArguments() {
    return arguments;
  }

  public void setArguments(List<Argument> arguments) {
    this.arguments = arguments;
  }

  public String getRefOutput() {
    return refOutput;
  }

  public void setRefOutput(String refOutput) {
    this.refOutput = refOutput;
  }

  public double getMaxPoints() {
    return maxPoints;
  }

  public void setMaxPoints(double maxPoints) {
    this.maxPoints = maxPoints;
  }

  public Assignment getAssignment() {
    return assignment;
  }

  public void setAssignment(Assignment assignment) {
    this.assignment = assignment;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  @Override
  public String toString() {
    return "Vector{" + "refOutput=" + refOutput + ", maxPoints=" + maxPoints + ", filename=" + filename + '}';
  }
}
