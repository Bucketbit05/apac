/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.support.MethodArgumentTypeMismatchException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

/**
 * \
 *
 * @author František Špaček
 */
@ControllerAdvice
public class GlobalExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler({ResourceNotFoundException.class, NullPointerException.class})
  @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Requested resource not found")
  public void handleNotFound(Exception ex, WebRequest request) {
    LOG.debug("Entity not found", ex);
  }

  @ExceptionHandler({InvalidFileException.class, IOException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Incorrect format of request body")
  public void handleBadRequest(Exception ex) {
    LOG.debug("Incorrect format of request body", ex);
  }

  @ExceptionHandler({UnableToRunException.class, ProcessorNotFoundException.class})
  @ResponseStatus(value = HttpStatus.EXPECTATION_FAILED,
      reason = "Submission compilation failed, unable to run")
  public void handleUnableToRun(Exception ex) {
    LOG.debug("Submission compilation failed, unable to run", ex);
  }

  @ExceptionHandler({FilenameCannotBeEmptyException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST,
      reason = "File is not zip, filename cannot be empty")
  public void handleFilenameCannotBeEmpty(Exception ex) {
    LOG.debug("File is not zip, filename cannot be empty", ex);
  }

  @ExceptionHandler(AssignmentIncompleteException.class)
  @ResponseStatus(value = HttpStatus.EXPECTATION_FAILED,
      reason = "Assignment is incomplete, unable to create submission")
  public void handleAssignmentIncomplete(Exception ex) {
    LOG.debug("Assignment is incomplete, unable to create submission", ex);
  }

  @ExceptionHandler(InvalidVectorException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Vector is not valid")
  public void handleInvalidVectorException(Exception ex) {
    LOG.debug("Vector is not valid", ex);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid path variable provided")
  public void handleTypeMismatch(Exception ex) {
    LOG.debug("Invalid path variable provided", ex);
  }

  @ExceptionHandler(UnsupportedOperationException.class)
  @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED,
      reason = "Request operation is not supported")
  public void handleUnsupportedOperation(Exception ex) {
    LOG.debug("Requested operation is not supported", ex);
  }
}
