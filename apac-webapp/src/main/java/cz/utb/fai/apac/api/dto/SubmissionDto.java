/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.utb.fai.apac.domain.Submission;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author František Špaček
 */
public class SubmissionDto extends BaseDto {

  private Double totalPoints;
  private String compilerOutput;
  private Boolean compileFailed;
  private String uuid;
  private Boolean reference;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Double similarity;

  List<SubmissionSimilarityDto> similarSubmissions = new ArrayList<>();

  public SubmissionDto() {
  }

  public SubmissionDto(String uuid) {
    this.uuid = uuid;
  }

  public static SubmissionDto fromEntity(Submission entity) {
    SubmissionDto dto = new SubmissionDto();

    dto.setCompileFailed(entity.getCompileFailed());
    dto.setReference(entity.getReference());
    dto.setCompilerOutput(entity.getCompileOutput());
    dto.setTotalPoints(entity.getTotalPoints());
    dto.setUuid(entity.getUuid());

    dto.setSimilarity(entity.getSimilarity() != null && entity.getSimilarity() > 0
        ? entity.getSimilarity()
        : null);

    dto.setSimilarSubmissions(entity.getSimilarSubmissions().stream()
        .map(SubmissionSimilarityDto::fromEntity)
        .collect(Collectors.toList()));

    return dto;
  }

  public static Submission toEntity(SubmissionDto dto) {
    Submission entity = new Submission();
    entity.setReference(dto.getReference());
    return entity;
  }

  public Double getTotalPoints() {
    return totalPoints;
  }

  public void setTotalPoints(Double totalPoints) {
    this.totalPoints = totalPoints;
  }

  public String getCompilerOutput() {
    return compilerOutput;
  }

  public void setCompilerOutput(String compilerOutput) {
    this.compilerOutput = compilerOutput;
  }

  public boolean isCompileFailed() {
    return compileFailed;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Boolean getCompileFailed() {
    return compileFailed;
  }

  public void setCompileFailed(Boolean compileFailed) {
    this.compileFailed = compileFailed;
  }

  public Boolean getReference() {
    return reference;
  }

  public void setReference(Boolean reference) {
    this.reference = reference;
  }

  public Double getSimilarity() {
    return similarity;
  }

  public void setSimilarity(Double similarity) {
    this.similarity = similarity;
  }

  public List<SubmissionSimilarityDto> getSimilarSubmissions() {
    return similarSubmissions;
  }

  public void setSimilarSubmissions(List<SubmissionSimilarityDto> similarSubmissions) {
    this.similarSubmissions = similarSubmissions;
  }

  @Override
  public String toString() {
    return "SubmissionDTO{" + "totalPoints=" + totalPoints
        + ", compilerOutput=" + compilerOutput + ", compileFailed="
        + compileFailed + ", uuid=" + uuid
        + ", reference=" + reference + '}';
  }

}
