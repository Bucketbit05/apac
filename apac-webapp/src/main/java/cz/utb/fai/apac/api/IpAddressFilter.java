/*
 * Copyright (C) 2015 František Špaček
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */

package cz.utb.fai.apac.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * IP address filter to allow access only from configured IP addresses.
 *
 * @author František Špaček
 */
@Component
public class IpAddressFilter implements Filter {

  private static final Logger LOG = LoggerFactory
      .getLogger(IpAddressFilter.class);

  @Value("${apac.security.ipWhitelist}")
  private String ipWhiteList;

  @Override
  public void init(FilterConfig fc) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp,
      FilterChain chain) throws IOException, ServletException {
    if (ipWhiteList == null || ipWhiteList.isEmpty()) {
      chain.doFilter(req, resp);
    } else {
      String ipAddress = req.getRemoteAddr();

      if (ipWhiteList.contains(ipAddress)) {
        chain.doFilter(req, resp);
      } else {
        LOG.info("Request from not allowed IP address {}", ipAddress);
        HttpServletResponse response = (HttpServletResponse) resp;
        response.sendError(HttpStatus.FORBIDDEN.value());
      }
    }
  }

  @Override
  public void destroy() {
  }

}
