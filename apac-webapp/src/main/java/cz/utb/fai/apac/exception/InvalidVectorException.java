/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.utb.fai.apac.exception;

/**
 * @author František Špaček
 */
public class InvalidVectorException extends RuntimeException {

    public InvalidVectorException() {
    }

    public InvalidVectorException(String message) {
        super(message);
    }

}
