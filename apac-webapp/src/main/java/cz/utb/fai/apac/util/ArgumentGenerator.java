/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.api.dto.ArgumentDto;
import cz.utb.fai.apac.enums.ArgumentType;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

/**
 *
 * @author František Špaček
 */
public class ArgumentGenerator {

  public static ArgumentDto generate(ArgumentType type, String name,
      int length) {
    ArgumentDto dto = new ArgumentDto();
    dto.setName(name.isEmpty()
        ? RandomStringUtils.randomAlphabetic(4).toLowerCase()
        : name);
    dto.setType(type);
    dto.setValue(generateValue(type, length));
    return dto;

  }

  public static String generateValue(ArgumentType type, int length) {
    String result = "";

    switch (type) {
      case ALPHABETIC:
        result = RandomStringUtils.randomAlphabetic(length);
        break;
      case ALPHANUMERIC:
        result = RandomStringUtils.randomAlphanumeric(length);
        break;
      case NUMERIC:
        result = RandomStringUtils.randomNumeric(length);
        break;
      case SHORT:
        result = String.valueOf((short) RandomUtils.nextInt());
        break;
      case DOUBLE:
        result = String.valueOf(RandomUtils.nextDouble());
        break;
      case INTEGER:
        result = String.valueOf(RandomUtils.nextInt());
        break;
      case LONG:
        result = String.valueOf(RandomUtils.nextLong());
        break;
      case STRING:
        result = String.valueOf(RandomStringUtils.randomAscii(length));
        break;
      case INTARRAY:
        result = generateArray(ArgumentType.INTEGER, length);
        break;
      case LONGARRAY:
        result = generateArray(ArgumentType.LONG, length);
        break;
      case STRINGARRAY:
        result = generateArray(ArgumentType.STRING, length);
        break;
      default:
        throw new UnsupportedOperationException("Requested type of argument is not supported");
    }
    return result;
  }

  public static String generateArray(ArgumentType type, int length) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < length; i++) {
      switch (type) {
        case INTEGER:
          sb.append(generateValue(type, length));
          break;
        case LONG:
          sb.append(generateValue(type, length));
          break;
        case STRING:
          sb.append(generateValue(type, length));
          break;
      }
      sb.append(" ");
    }

    return sb.toString();
  }
}
