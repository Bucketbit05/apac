/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import cz.utb.fai.apac.commons.enums.Language;
import cz.utb.fai.apac.exception.FilenameCannotBeEmptyException;
import cz.utb.fai.apac.exception.InvalidFileException;

import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * @author František Špaček
 */
public class UploadUtil {

  private static final Tika TIKA = new Tika();
  private static final Logger LOG = LoggerFactory.getLogger(UploadUtil.class);

  public static MediaType detectType(File file) throws IOException {
    return MediaType.parse(TIKA.detect(file));
  }

  public static MediaType detectType(byte[] bytes) throws IOException {
    return MediaType.parse(TIKA.detect(bytes));
  }

  public static File processUpload(byte[] bytes, String uploadPath, String filename) throws IOException {
    MediaType mediaType = MediaType.parse(TIKA.detect(bytes, filename));
    LOG.info("Media type of uploaded file is {}", mediaType);

    boolean supportType = mediaType.equals(MediaType.APPLICATION_ZIP);

    if (!supportType) {
      supportType = Language.isSupported(mediaType.toString());

      if (supportType && filename.isEmpty()) {
        throw new FilenameCannotBeEmptyException();
      }
    }

    if (!supportType) {
      throw new InvalidFileException("Uploaded file type is not supported");
    }

    File uploadedFile = new File(DirUtil.fixPath(uploadPath)
        + (filename.isEmpty() ? System.currentTimeMillis() : filename));
    FileUtils.writeByteArrayToFile(uploadedFile, bytes);

    return uploadedFile;
  }
}
