/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

import cz.utb.fai.apac.docker.client.DockerClient;
import cz.utb.fai.apac.docker.client.UnixDockerClientImpl;
import cz.utb.fai.apac.monitoring.LoggingAspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import java.io.File;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;
import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

@Configuration
@ComponentScan
@EnableSwagger
@EnableJpaRepositories
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

  @Autowired
  private ConnectionFactory connectionFactory;

  private SpringSwaggerConfig springSwaggerConfig;

  public static void main(String[] args) {
    FileSystemUtils.deleteRecursively(new File("activemq-data"));
    SpringApplication.run(Application.class, args);

  }

  @Bean
  public DockerClient dockerClient(@Value("${apac.docker.hostname}") String socketPath) {
    return new UnixDockerClientImpl(socketPath);
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer
      propertyPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Override
  protected SpringApplicationBuilder configure(
      SpringApplicationBuilder application) {
    return application.sources(Application.class);
  }

  @Bean
  @Autowired
  DefaultMessageListenerContainer messageListener(
      MessageListener submissionListener, @Value("${apac.jms.destination}") String jmsDestination) {
    DefaultMessageListenerContainer container
        = new DefaultMessageListenerContainer();
    container.setMessageListener(submissionListener);
    container.setConnectionFactory(connectionFactory);
    container.setDestinationName(jmsDestination);
    container.setCacheLevel(DefaultMessageListenerContainer.CACHE_SESSION);
    container.setConcurrentConsumers(5);
    container.setSessionTransacted(true);
    return container;
  }

  @Bean
  public Filter loggingFilter() {
    AbstractRequestLoggingFilter filter = new AbstractRequestLoggingFilter() {

      private final Logger LOG = LoggerFactory
          .getLogger(AbstractRequestLoggingFilter.class);

      @Override
      protected void beforeRequest(HttpServletRequest request,
          String message) {
        LOG.info(message.replace("null", ""));
      }

      @Override
      protected void afterRequest(HttpServletRequest request,
          String message) {
        LOG.info(message.replace("null", ""));
      }
    };
    filter.setIncludeClientInfo(true);
    filter.setIncludeQueryString(true);
    filter.setBeforeMessagePrefix("Processing request  [");
    filter.setAfterMessagePrefix("Request finished   [");
    filter.setAfterMessageSuffix("]\n");
    return filter;
  }

  @Autowired
  public void setSpringSwaggerConfig(
      SpringSwaggerConfig springSwaggerConfig) {
    this.springSwaggerConfig = springSwaggerConfig;
  }

  @Bean
  public SwaggerSpringMvcPlugin customImplementation() {
    return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfo("APAC", "Automatic Programming Assignment Checker",
        "", "spacek@fai.utb.cz", "GPLv3",
        "https://www.gnu.org/copyleft/gpl.html");
  }

  @Bean
  public LoggingAspect loggingAspect() {
    return new LoggingAspect();
  }
}
