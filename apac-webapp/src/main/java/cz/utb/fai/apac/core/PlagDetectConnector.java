/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.core;

import static cz.utb.fai.apac.util.WorkspaceUtil.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.apac.commons.enums.Extension;
import cz.utb.fai.apac.domain.Assignment;
import cz.utb.fai.apac.domain.Submission;
import cz.utb.fai.apac.repository.SubmissionRepository;
import cz.utb.fai.apac.service.AssignmentService;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.zeroturnaround.zip.ZipUtil;
import org.zeroturnaround.zip.commons.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author František Špaček
 */
@Component
public class PlagDetectConnector {

  private static final Logger LOG = LoggerFactory
      .getLogger(PlagDetectConnector.class);

  @Autowired
  private AssignmentService assignmentService;

  @Autowired
  private SubmissionRepository submissionRepository;

  @Value("${plagdetector.api.submissions}")
  private String submissionApiUrl;

  @Value("${plagdetector.api.assignments}")
  private String assignmentApiUrl;

  @Value("${apac.temp}")
  private String basePath;

  private CloseableHttpClient client;

  public PlagDetectConnector() {
    RequestConfig requestConfig = RequestConfig.custom()
        .setSocketTimeout(1000)
        .setConnectTimeout(1000)
        .setConnectionRequestTimeout(1000)
        .build();

    PoolingHttpClientConnectionManager cm
        = new PoolingHttpClientConnectionManager();
    cm.setMaxTotal(50);
    cm.setDefaultMaxPerRoute(20);
    client = HttpClients.custom()
        .setDefaultRequestConfig(requestConfig)
        .setConnectionManager(cm)
        .build();
  }

  @Scheduled(fixedRate = 5 * 60 * 1000)
  public void send() {
    List<Submission> submissions = submissionRepository
        .findAllForPlagiarismCheck(LocalDateTime.now());

    boolean plagDetectorAvailable = true;
    for (Submission s : submissions) {
      if (!plagDetectorAvailable) {
        break;
      }

      String assignmentPlagId = s.getAssignment().getAssignmentPlagId();
      if (StringUtils.isEmpty(assignmentPlagId)) {
        try {
          LOG.info("Creating assignment in plag detector");
          assignmentPlagId = createAssignment(s.getAssignment().getId());
          LOG.info("Assignment created with id {}", assignmentPlagId);
        } catch (JsonProcessingException ex) {
          LOG.error("Error during assignment processing to plag detector", ex);
        }
      }

      try {
        plagDetectorAvailable = processSubmission(assignmentPlagId,
            workspace(basePath, s.getUuid()), s.getOwner());

      } catch (JsonProcessingException ex) {
        LOG.error("Error during assignment processing to plag detector", ex);
      }
    }
  }

  private String createAssignment(long assignmentId)
      throws JsonProcessingException {
    String plagAssignmentId = "";

    Assignment assignment = assignmentService.get(assignmentId);
    DetectorAssignment pAssignment = new DetectorAssignment(
        assignment.getName(), assignment.getLang());

    StringEntity entity;
    entity = new StringEntity(new ObjectMapper()
        .writeValueAsString(pAssignment), ContentType.APPLICATION_JSON);
    HttpPost post = new HttpPost(assignmentApiUrl);
    post.setEntity(entity);

    try (CloseableHttpResponse response = client.execute(post)) {

      if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
        pAssignment = new ObjectMapper()
            .readValue(EntityUtils.toString(response.getEntity()),
                DetectorAssignment.class);
        assignment.setAssignmentPlagId(pAssignment.getAssignmentId());
        assignmentService.update(assignment);

        plagAssignmentId = pAssignment.getAssignmentId();
      } else {
        LOG.info("Response from plag detector has status code {}",
            response.getStatusLine().getStatusCode());
      }

    } catch (IOException ex) {
      LOG.error("Error during assignment processing to plag detector", ex);
    }

    return plagAssignmentId;
  }

  private boolean processSubmission(String assignmentId, File workspace,
      String owner)
      throws JsonProcessingException {
    LOG.info("Processing submission for plagiarism check for {}",
        workspace.getName());

    boolean plagDetectorAvailable = true;
    if (StringUtils.isEmpty(assignmentId)) {
      LOG.info("Plagiarism assignment not found, end of plagiarism check");
      return plagDetectorAvailable;
    }

    File zipFile = new File(workspace.getAbsolutePath()
        + File.separator + workspace.getName() + ".zip");

    File[] oldZipFiles = workspace.listFiles((File dir, String name)
        -> name.endsWith(".zip"));

    if (oldZipFiles == null) {
      return false;
    }

    for (File f : oldZipFiles) {
      FileUtils.deleteQuietly(f);
    }

    File[] sourceFiles = workspace.listFiles((File dir, String name)
        -> !(name.endsWith(Extension.BUILD.toString())
        || name.endsWith(Extension.IN.toString())
        || name.endsWith(Extension.OUT.toString())));

    ZipUtil.packEntries(sourceFiles, zipFile);

    SubmissionMeta meta = new SubmissionMeta();
    meta.setAssignmentId(assignmentId);
    meta.setOwner(owner.isEmpty() ? workspace.getName() : owner);
    meta.setIdentificator(workspace.getName());

    HttpPost post = new HttpPost(submissionApiUrl);
    String boundary = "-------------" + System.currentTimeMillis();
    post.setHeader("Content-type", "multipart/form-data; boundary=" + boundary);

    MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
    entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
    entityBuilder.setBoundary(boundary);
    entityBuilder.addBinaryBody("submission-data", zipFile,
        ContentType.create("application/zip"), zipFile.getName());

    entityBuilder.addTextBody("submission-meta", new ObjectMapper()
        .writeValueAsString(meta));
    post.setEntity(entityBuilder.build());

    try (CloseableHttpResponse response = client.execute(post)) {
      LOG.info("Submission {} submitted to plag detector with status code {}",
          workspace.getName(), response.getStatusLine().getStatusCode());
    } catch (JsonProcessingException ex) {
      LOG.error("Error during creating submission meta for plag detector", ex);
    } catch (IOException ex) {
      LOG.error("Error during submission processing to plag detector", ex);
      plagDetectorAvailable = false;
    }

    return plagDetectorAvailable;
  }

  private static class SubmissionMeta {

    private String assignmentId;
    private String owner;
    private String identificator;

    public String getAssignmentId() {
      return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
      this.assignmentId = assignmentId;
    }

    public String getOwner() {
      return owner;
    }

    public void setOwner(String owner) {
      this.owner = owner;
    }

    public String getIdentificator() {
      return identificator;
    }

    public void setIdentificator(String identificator) {
      this.identificator = identificator;
    }
  }

  private static class DetectorAssignment {

    @JsonProperty("ID")
    private String assignmentId;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Lang")
    private String lang;

    public DetectorAssignment() {
    }

    public DetectorAssignment(String name, String lang) {
      this.name = name;
      this.lang = lang;
    }

    public String getAssignmentId() {
      return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
      this.assignmentId = assignmentId;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getLang() {
      return lang;
    }

    public void setLang(String lang) {
      this.lang = lang;
    }
  }
}
