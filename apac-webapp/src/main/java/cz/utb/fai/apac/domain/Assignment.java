/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author František Špaček
 */
@Entity
@Table(name = "assignments")
public class Assignment extends BaseEntity {

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String lang;

  @Column(nullable = false)
  private Integer allowedCompileWarnings = 0;

  @Column(nullable = false)
  private double compilationWeight = 0.1;

  @Column(nullable = false)
  private double executionWeight = 0.9;

  @Column(length = 1000)
  private String description;

  /**
   * max runtime in milis
   */
  @Column(nullable = false)
  private Integer maxRuntime = 10000;

  /**
   * max submission size in MB
   */
  @Column(nullable = false)
  private Integer maxSize = 1;

  private String basefile;

  @Column(nullable = false)
  private Integer threshold = 60;

  @Column(nullable = false)
  private boolean createdReference = false;

  @OneToMany(mappedBy = "assignment", fetch = FetchType.EAGER)
  private List<Vector> vectors = new ArrayList<>();

  @OneToMany(mappedBy = "assignment")
  private List<Submission> submissions = new ArrayList<>();

  private LocalDateTime validFrom;
  private LocalDateTime validTo;

  private String assignmentPlagId;

  private boolean enableNetworking = true;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getMaxRuntime() {
    return maxRuntime;
  }

  public void setMaxRuntime(Integer maxRuntime) {
    this.maxRuntime = maxRuntime;
  }

  public Integer getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(Integer maxSize) {
    this.maxSize = maxSize;
  }

  public String getBasefile() {
    return basefile;
  }

  public void setBasefile(String basefile) {
    this.basefile = basefile;
  }

  public Integer getThreshold() {
    return threshold;
  }

  public void setThreshold(Integer threshold) {
    this.threshold = threshold;
  }

  public List<Vector> getVectors() {
    return vectors;
  }

  public void setVectors(List<Vector> vectors) {
    this.vectors = vectors;
  }

  public List<Submission> getSubmissions() {
    return submissions;
  }

  public void setSubmissions(List<Submission> submissions) {
    this.submissions = submissions;
  }

  public Integer getAllowedCompileWarnings() {
    return allowedCompileWarnings;
  }

  public void setAllowedCompileWarnings(Integer allowedCompileWarnings) {
    this.allowedCompileWarnings = allowedCompileWarnings;
  }

  public double getCompilationWeight() {
    return compilationWeight;
  }

  public void setCompilationWeight(double compilationWeight) {
    this.compilationWeight = compilationWeight;
  }

  public double getExecutionWeight() {
    return executionWeight;
  }

  public void setExecutionWeight(double executionWeight) {
    this.executionWeight = executionWeight;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDateTime validFrom) {
    this.validFrom = validFrom;
  }

  public LocalDateTime getValidTo() {
    return validTo;
  }

  public void setValidTo(LocalDateTime validTo) {
    this.validTo = validTo;
  }

  public String getAssignmentPlagId() {
    return assignmentPlagId;
  }

  public void setAssignmentPlagId(String assignmentPlagId) {
    this.assignmentPlagId = assignmentPlagId;
  }

  public boolean isCreatedReference() {
    return createdReference;
  }

  public void setCreatedReference(boolean createdReference) {
    this.createdReference = createdReference;
  }

  public boolean isEnableNetworking() {
    return enableNetworking;
  }

  public void setEnableNetworking(boolean enableNetworking) {
    this.enableNetworking = enableNetworking;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 73 * hash + Objects.hashCode(this.name);
    hash = 73 * hash + Objects.hashCode(this.lang);
    hash = 73 * hash + Objects.hashCode(this.allowedCompileWarnings);
    hash = 73 * hash + (int) (Double.doubleToLongBits(this.compilationWeight)
        ^ (Double.doubleToLongBits(this.compilationWeight) >>> 32));
    hash = 73 * hash + (int) (Double.doubleToLongBits(this.executionWeight)
        ^ (Double.doubleToLongBits(this.executionWeight) >>> 32));
    hash = 73 * hash + Objects.hashCode(this.description);
    hash = 73 * hash + Objects.hashCode(this.maxSize);
    hash = 73 * hash + Objects.hashCode(this.basefile);
    hash = 73 * hash + Objects.hashCode(this.threshold);
    hash = 73 * hash + Objects.hashCode(this.createdReference);
    hash = 73 * hash + Objects.hashCode(this.validFrom);
    hash = 73 * hash + Objects.hashCode(this.validTo);
    hash = 73 * hash + Objects.hashCode(this.assignmentPlagId);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Assignment other = (Assignment) obj;
    if (!Objects.equals(this.name, other.name)) {
      return false;
    }
    if (!Objects.equals(this.lang, other.lang)) {
      return false;
    }
    if (!Objects.equals(this.allowedCompileWarnings, other.allowedCompileWarnings)) {
      return false;
    }
    if (Double.doubleToLongBits(this.compilationWeight) != Double.doubleToLongBits(other.compilationWeight)) {
      return false;
    }
    if (Double.doubleToLongBits(this.executionWeight) != Double.doubleToLongBits(other.executionWeight)) {
      return false;
    }
    if (!Objects.equals(this.description, other.description)) {
      return false;
    }
    if (!Objects.equals(this.maxRuntime, other.maxRuntime)) {
      return false;
    }
    if (!Objects.equals(this.maxSize, other.maxSize)) {
      return false;
    }
    if (!Objects.equals(this.basefile, other.basefile)) {
      return false;
    }
    if (!Objects.equals(this.threshold, other.threshold)) {
      return false;
    }
    if (!Objects.equals(this.createdReference, other.createdReference)) {
      return false;
    }
    if (!Objects.equals(this.validFrom, other.validFrom)) {
      return false;
    }
    if (!Objects.equals(this.validTo, other.validTo)) {
      return false;
    }
    return Objects.equals(this.assignmentPlagId, other.assignmentPlagId);
  }

  @Override
  public String toString() {
    return "Assignment{" + "name=" + name + ", lang=" + lang + ", allowedCompileWarnings="
        + allowedCompileWarnings + ", compilationWeight=" + compilationWeight + ", executionWeight="
        + executionWeight + ", description=" + description + ", maxRuntime=" + maxRuntime + ", maxSize="
        + maxSize + ", basefile=" + basefile + ", threshold=" + threshold + ", createdReference="
        + createdReference + ", validFrom=" + validFrom + ", validTo=" + validTo + ", assignmentPlagId="
        + assignmentPlagId + '}';
  }

}
